# Go Server

## Summary
a simple HTTP server for the mixnn SGX app.

features 2 endpoint : 
- /upload : POST, the body of the request will be saved inside ./batch/<current_timestamp>.bin and will be then processed by the mixnn app if running.
- /update : POST, used to mimick a target aggregator server. nothing sent to that endpoint will be saved or processed, but the server will write in console output the timestamp when it was received. Used in our performance testing.

## Usage 
either start it using : 
```sh
go run ./server.go
```

or build it then run : 
```sh
go build .server.go
./server
```

the listening port of the server can be specified using `-p` e.g. :
```sh
./server -p 9191
```
will start a server listening on port 9191.


