#include "watchhandler.h"
#include "enclavemanager.h"
#include <iostream>
#include <thread>
#include <dirent.h>
#include <algorithm>

using namespace std;

void handleInThread(string path){
    EnclaveManager::process(path);
}

WatchHandler::WatchHandler(string path, int nThread){
    this->path = path;
    this->pool = new ThreadPool(nThread);
    if(!EnclaveManager::getManager()->isInitialized()){
        EnclaveManager::init();
        printf("Enclave Initialized !\n");
    }
}

string WatchHandler::fullPath(string filename){
    return (path + filename);
}

bool WatchHandler::handle(InotifyEvent &e){
    eventCount++;
    pool->enqueue(handleInThread, this->fullPath(e.getName()));
    if(eventCount > 10) 
        return true;
    return stopped;
}

WatchHandler::~WatchHandler(){
    stopped = true;
    pool->exit([]()-> void {});
    delete pool;
}