#ifndef WATCHHANDLER_H_
#define WATCHHANDLER_H_
#include <InotifyManager.h>
#include <threadpool.h>
#include <iostream>
#include <mutex>
#include <string>

using namespace std;

class WatchHandler : public InotifyEventHandler {
    private:
        int eventCount = 0;
        ThreadPool *pool;
        bool stopped = false;
        string path;
        string fullPath(string filename);
        set<string> doneFiles;
    public:
        WatchHandler(string path, int nThread);
        ~WatchHandler();
        bool handle(InotifyEvent &e);
};   

#endif