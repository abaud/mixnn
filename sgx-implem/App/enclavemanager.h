#ifndef ENCLAVEMANAGER_H_
#define ENCLAVEMANAGER_H_
#include "Enclave_u.h"
#include "sgx_utils/sgx_utils.h"
#include <string>

using namespace std;

class EnclaveManager{
    private:
        sgx_enclave_id_t global_eid = 0;
        bool initialized = false;
    protected:
        EnclaveManager();
        static EnclaveManager* enclaveManager_;
        void initInternal();
        void processInternal(string path);
        int generateKeysInternal();
    public:
        /**
         * Singletons should not be cloneable.
         */
        EnclaveManager(EnclaveManager &other) = delete;
        sgx_enclave_id_t getEid(){ return this->global_eid;};
        static sgx_enclave_id_t getInstanceEid(){return getManager()->getInstanceEid();};
        /**
         * Singletons should not be assignable.
         */
        void operator=(const EnclaveManager &) = delete;
        static EnclaveManager* getManager();
        static void init();
        static void process(string path);
        static int generateKeys();
        bool isInitialized() { return this->initialized; };
};

#endif