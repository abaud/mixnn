#include "filewatcher.h"
#include <thread>
#include <string>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include "utils/config.h"

using namespace std;

FileWatcher::FileWatcher(string path, int nThread){
    this->path = path;
    DIR* dir = opendir(path.c_str());
    if(dir) {
        /* Directory exists. */
        closedir(dir);
    } else if (ENOENT == errno) {
        mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    } else {
        std::cout << "cannot create data error:" << strerror(errno) << std::endl;
        throw std::runtime_error( strerror(errno) );
    }
    this->manager = new InotifyManager();
    this->watch = this->manager->addWatch(path, IN_CLOSE_WRITE);
    this->handler = new WatchHandler(path, nThread);
    this->watch->addEventHandler(*handler);
}

void FileWatcher::run(){
    t = manager->startWatching();
    t.join();
}
FileWatcher::~FileWatcher(){
    delete handler;
    delete manager;
}