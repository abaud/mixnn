#ifndef MESSAGE_H_
#define MESSAGE_H_
#include <stdint.h>
#include <stddef.h>
#include <string>

class Message{
  private :
    std::string m_filename;
    char *m_content;
    size_t m_length;
    size_t get_file_length();
  public : 
    Message(std::string filename);
    char *getContent();
    void loadContent();
    size_t getLength(){ return m_length; }
    bool deleteFile();
    ~Message();
};

void retrieve_full_message(char *message, size_t size, char *filename);

#endif // MESSAGE_H_