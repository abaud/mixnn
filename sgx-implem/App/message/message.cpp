#include "message.h"
#include <stdint.h>
#include <stddef.h>
#include <sys/stat.h>
#include <fstream>
#include <string>

using namespace std;

Message::Message(std::string filename){
    this->m_filename = filename;
    this->m_content = nullptr;
    this->m_length = get_file_length();
}

char* Message::getContent(){
    if(this->m_content != NULL){
        return this->m_content;
    }
    this->loadContent();
    return this->m_content;
}

void Message::loadContent(){
    this->m_content = (char*)malloc(this->m_length + 1);
    ifstream file(this->m_filename, ios::in);
    if (file.fail()) {
        printf("FAIL\n");
        return;
    }
    file.read(this->m_content, this->m_length);
    //file.close();
}

size_t Message::get_file_length(){
      struct stat st;
      stat(this->m_filename.c_str(), &st);
      size_t size = st.st_size;
      return size;
}

Message::~Message(){
    if(m_content != nullptr){
        free(this->m_content);
        this->m_content = nullptr;
    }
}

bool Message::deleteFile(){
    return !remove(m_filename.c_str());
}
