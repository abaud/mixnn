#include "enclavemanager.h"
#include <iostream>
#include <stdexcept>
#include "message/message.h"
#include <thread>
#include <ctime>
#include <ratio>
#include <chrono>
#include "utils/utils.h"
#include "utils/config.h"

using namespace std::chrono;

EnclaveManager* EnclaveManager::enclaveManager_ = nullptr;

EnclaveManager::EnclaveManager(){
     if (initialize_enclave(&this->global_eid, "enclave.token", "enclave.signed.so") < 0) {
        std::cout << "Fail to initialize enclave." << std::endl;
        throw std::runtime_error("Fail to initialize enclave");
    }
    printf("Enclave Initialized!\n");
}

EnclaveManager* EnclaveManager::getManager(){
    if(enclaveManager_ == nullptr){
        enclaveManager_ = new EnclaveManager();
    }
    return enclaveManager_;
}

void EnclaveManager::initInternal(){
    ConfigManager *cfg = ConfigManager::getManager();
    ecall_init(global_eid, cfg->threshold(), cfg->useRepoHandler(), cfg->useUpdateRepo());
    this->initialized = true;
}

int EnclaveManager::generateKeysInternal(){
    int ret;
    ecall_rsa_gen(global_eid, &ret);
    return ret;
}

void EnclaveManager::processInternal(string path){
    Message *message;
    message = new Message(path);
    message->loadContent();
    
    switch(ConfigManager::getManager()->mode()){
        case ConfigManager::PROXY :
            postFileHTTP((uint8_t*)message->getContent(), message->getLength());
            break;
        case ConfigManager::MIXNN :
            decryptAndAdd(global_eid, message->getContent());
            break;
        case ConfigManager::AVERAGE :
            decryptAndAvg(global_eid, message->getContent());
            break;
        case ConfigManager::DECRYPT :
            decryptOnly(global_eid, message->getContent());
            break;
        case ConfigManager::MEDIAN :
            decryptAndMedian(global_eid, message->getContent());
            break;

    }

    message->deleteFile();
    delete message;
}

void EnclaveManager::init(){
    (EnclaveManager::getManager())->initInternal();
}


int EnclaveManager::generateKeys(){
    return (EnclaveManager::getManager())->generateKeysInternal();
}

void EnclaveManager::process(string path){
    (EnclaveManager::getManager())->processInternal(path);
}

