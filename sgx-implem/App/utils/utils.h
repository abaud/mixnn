#ifndef UNTRUSTED_UTILS_H_
#define UNTRUSTED_UTILS_H_
#include <stdint.h>
#include <stddef.h>

void postFileHTTP(const uint8_t *data, const size_t size);

#endif