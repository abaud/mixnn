#include "utils.h"
#include <fstream>
#include <iostream>
#include <byteStream.h>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>
#include "config.h"

void postFileHTTP(const uint8_t *data, const size_t size){
    ConfigManager *cfg = ConfigManager::getManager();
    try {
        curlpp::Cleanup cleaner;
        curlpp::Easy request;

        std::list<std::string> headers;
        headers.push_back("Content-Type: application/octet-stream"); 

        using namespace curlpp::Options;
		request.setOpt(new Post(true));
        request.setOpt(new PostFieldSizeLarge(size));
        memstream s(data, size);
        request.setOpt(new ReadStream(&s));
		request.setOpt(new HttpHeader(headers));
		request.setOpt(new Url(cfg->url()));

        std::ofstream myfile("/dev/null");
        myfile << request;
    }
    catch ( curlpp::LogicError & e ) {
        std::cout << "LOGICERR:" << e.what() << std::endl;
    }
    catch ( curlpp::RuntimeError & e ) {
        std::cout << "RUNTIMEERR:" << e.what() << std::endl;
    }
}