#ifndef CONFIGMANAGER_H_
#define CONFIGMANAGER_H_

#include <string>

class ConfigManager{
    public:
        enum eMode{
            PROXY,
            MIXNN,
            AVERAGE,
            DECRYPT,
            MEDIAN
        };
        /**
         * Singletons should not be cloneable.
         */
        ConfigManager(ConfigManager &other) = delete;
        /**
         * Singletons should not be assignable.
         */
        void operator=(const ConfigManager &) = delete;
        static ConfigManager* getManager();
        int nThread(){ return nThread_; }
        std::string url(){ return url_; }
        std::string inputFolder(){return inputFolder_; }
        eMode mode(){return mode_; }
        int waitTime(){ return waitTime_; }
        bool useWait(){ return useWait_; }
        int threshold(){ return threshold_; }
        bool useUpdateRepo();
        bool useRepoHandler();
        void printConfig();
    protected:
        ConfigManager();
        static ConfigManager* configManager_;
    private:
        void readConfigFile();
        int nThread_;
        std::string url_;
        std::string inputFolder_;
        eMode mode_;
        int waitTime_;
        bool useWait_;
        bool initialized_;
        int threshold_;
};

#endif