#include "config.h"
#include <iostream>
#include <fstream>
#include <sstream>

ConfigManager* ConfigManager::configManager_ = nullptr;

ConfigManager::ConfigManager(){
    this-> url_ = "http://localhost:8181/update";
    this->inputFolder_ = "./batch/";
    this->nThread_ = 1;
    this->mode_ = PROXY;
    this->waitTime_ = 1000;
    this->useWait_ = true;
    this->readConfigFile();
    this->printConfig();
}

void ConfigManager::printConfig(){
    std::cout << "[CONFIG] Sending update to " << this->url_ << std::endl;
    std::cout << "[CONFIG] Watching " << this->inputFolder_ << " for update files" << std::endl;
    std::cout << "[CONFIG] Using " << this->nThread_ << " Thread(s)" << std::endl;
    switch(this->mode_){
        case PROXY:
            std::cout << "[CONFIG] Proxy" <<  std::endl;
            break;
        case MIXNN:
            std::cout << "[CONFIG] Using Mix" <<  std::endl;
            break;
        case AVERAGE:
            std::cout << "[CONFIG] Averaging" <<  std::endl;
            break;
        case DECRYPT: 
            std::cout << "[CONFIG] Decrypt" << std::endl;
            break;
        case MEDIAN:
            std::cout << "[CONFIG] Median" << std::endl;
    }
    
    if(this->useWait_)
        std::cout << "[CONFIG] Waiting " << this->waitTime_ << "ms between every check for new update." << std::endl;
    else
        std::cout << "[CONFIG] Using INotify to get new files." << std::endl;

    std::cout << "[CONFIG] Pool Size = "<< threshold_ << std::endl;
}

bool ConfigManager::useRepoHandler(){
    return this->mode_ == MIXNN;
}

bool ConfigManager::useUpdateRepo(){
    return (this->mode_ == AVERAGE || this->mode_ == MEDIAN);
}

void ConfigManager::readConfigFile(){
    std::ifstream cfg("config");
    std::string line;
    while (std::getline(cfg, line)){
        std::istringstream is_line(line);
        std::string key;
        if(std::getline(is_line, key, '=')){
            std::string value;
            if(std::getline(is_line, value)){
                if(key == "url"){
                    this->url_ = value;
                }else if(key == "input"){
                    this->inputFolder_ = value;
                }else if(key == "threads"){
                    this->nThread_ = std::stoi(value);
                }else if (key == "mode"){
                    this->mode_ = eMode(std::stoi(value));
                }else if (key == "wait"){
                    this->waitTime_ = std::stoi(value);
                }else if (key == "usewait"){
                    this->useWait_ = std::stoi(value);
                }else if (key == "threshold"){
                    this->threshold_ = std::stoi(value);
                }else{
                    std::cout << "[WARN] Config file contains an unknown key." << std::endl;
                }
            }
        }
    }
}

ConfigManager* ConfigManager::getManager(){
    if(configManager_ == nullptr){
        configManager_ = new ConfigManager();
    }
    return configManager_;
}