#ifndef FILEWATCHER_H_
#define FILEWATCHER_H_
#include <string>
#include <iostream>
#include <thread>
#include <InotifyManager.h>
#include "watchhandler.h"

using namespace std;

class FileWatcher {
    private:
        string path;
        WatchHandler *handler;
        InotifyManager *manager;
        InotifyWatch *watch;
        thread t;
    public:
        FileWatcher(string path, int nThread);
        ~FileWatcher();
        void run();
};

#endif