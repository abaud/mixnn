#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <thread>
#include <threadpool.h>
#include <base64.h>
#include <sgx_urts.h>
#include <fstream>
#include <iostream>
#include <string>
#include <iostream>
#include <ctime>
#include <ratio>
#include <chrono>
#include <dirent.h>
#include <signal.h>
#include "filewatcher.h"
#include "enclavemanager.h"
#include "ocall/ocall.h"
#include "utils/config.h"

using namespace std;
using namespace std::chrono;
using clock_type = std::chrono::high_resolution_clock;
FileWatcher *fw;

void intHandler(int) {
    delete fw;
}

//* Main Binary
int main(int argc, char const *argv[]) {

    struct sigaction act;
    act.sa_handler = intHandler;
    sigaction(SIGINT, &act, NULL);

    ConfigManager *cfg = ConfigManager::getManager();
    EnclaveManager::init();
    string watchpath = cfg->inputFolder();
    if(argc >= 2){
        if(!strcmp(argv[1], "-g")) {
            printf("Generating keys...\n");
            EnclaveManager::generateKeys();
            printf("Done !\nprivate key : %s\npublic key : %s\n", PRIVATE_FILE, PUBLIC_FILE);
        }else if(!strcmp(argv[1], "-h")){
            printf("MixNN Enclave\n");
            printf("Watch a directory for file creation and decipher and add its content inside an SGX enclave.\n\n");
            printf("Options:\n");
            printf("\t-g : generate key needed to encrypt file that will be added to the Enclave.\n");
            printf("\t-h show this help message\n\n");
            return 0;
        }else{
            printf("wrong usage, ./app -h to show help message\n");
            return -1;
        }
    }

    if(cfg->useWait()){
        ThreadPool *tp = new ThreadPool(cfg->nThread());
        set<string> doneFiles;
        auto interval = std::chrono::milliseconds(cfg->waitTime());
        auto when_started = clock_type::now(); 
        auto target_time = when_started + interval;
        while(1){
            DIR *dir; struct dirent *diread;
            vector<string> files;
            if ((dir = opendir(watchpath.c_str())) != nullptr) {
                while ((diread = readdir(dir)) != nullptr) {
                    if (strcmp(diread->d_name, ".") && strcmp(diread->d_name, "..") && !(doneFiles.find(diread->d_name) != doneFiles.end())){
                        struct stat st;
                        stat((watchpath + diread->d_name).c_str(), &st);
                        if(st.st_size == 4361111){
                            files.push_back(diread->d_name);
                            doneFiles.insert(diread->d_name);
                        }
                    }
                }
                closedir (dir);
            } else {
                perror ("opendir");
                return EXIT_FAILURE;
            }
            for (auto file : files){
                tp->enqueue(EnclaveManager::process, (watchpath + file));
            }
            std::this_thread::sleep_until(target_time);
            target_time += interval;
        }
    }else{
        fw = new FileWatcher(cfg->inputFolder(), cfg->nThread());
        fw->run();
    }
    sgx_destroy_enclave(EnclaveManager::getInstanceEid());
}
