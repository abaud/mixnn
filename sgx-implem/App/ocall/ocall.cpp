#include "ocall.h"
#include "Enclave_u.h"


#include <fstream>
#include <iostream>
#include <chrono>
#include <sstream>
#include <mutex>

#include "utils/utils.h"

using namespace std;
using namespace std::chrono;

mutex mtx;
int count = 0;

int load_private_key(uint8_t *sealed_data, size_t sealed_size) {
    ifstream file(PRIVATE_FILE, ios::in | ios::binary);
    if (file.fail()) {
        return 1;
    }
    file.read((char *)sealed_data, sealed_size);
    file.close();
    return 0;
}

int save_private_key(const uint8_t *sealed_data, const size_t sealed_size) {
    ofstream file(PRIVATE_FILE, ios::out | ios::binary);
    if (file.fail()) {
        return 1;
    }
    file.write((const char *)sealed_data, sealed_size);
    file.close();
    return 0;
}

int save_public_key(const char *data, const size_t size) {
    ofstream file(PUBLIC_FILE, ios::out | ios::binary);
    if (file.fail()) {
        return 1;
    }
    file.write((const char *)data, size);
    file.close();
    return 0;
}

void save_update_layer(const uint8_t *data, const size_t size) {
    //postFileHTTP(data, size);
}

void uprint(const char *str) {
    /* Proxy/Bridge will check the length and null-terminate 
     * the input string to prevent buffer overflow. 
     */
    printf("ENCLAVE : %s", str);
    fflush(stdout);
}