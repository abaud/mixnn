# mixnn SGX

## Build
for the SGX application :
```sh
make
```
for a hardware debug build of the application.
other mode can be choosen using variables :
```sh
make SGX_MODE=SIM SGX_DEBUG=0
```

the go server: 
```sh
go build ./server/server.go
```
or running directly using
```sh
go run ./server/server.go
```

## Config
a config file for the SGX application is located at `./config`:
```ini
url=http://192.168.1.54:8181/update
input=./batch/
threads=1
mode=1
wait=500
usewait=0
maxitems=10
```
- url : the url to the target aggregation server. Once the server received enough user update it will start sending mixed (or averaged) update to that endpoint.
- input : the folder in which the incoming user update will be stored. This folder will be watched to wait for any new update to process.
- threads : number of threads inside the enclave. (need to also be changed in Enclave.config.xml if increased)
- mode : what the application does with the received update : 
    - 0 Proxy : the update aren't going through the enclave, just directly redirected to the target server
    - 1 Mixnn
    - 2 Average : the enclave compute an average of the received updates and send it to the target server 
    - 3 Decrypt only : the enclave decrypt the received update and send it to the target server without any further process
    - 4 median : the enclave compute the median of the received updates (Really slow /!\ )

## Usage
### Server
you can change the listening port of the go server : 
```
go run ./server/server.go -p 8081
```
if not specified, the server listen on 8181

### SGX App

first run : 
```
./app -g
```
this will generate the necessary RSA keys.
subsequent run (using the same build) : 
```
./app
```
depending on how it was built (in debug mode or not) the rsa keys will be stored either to : 
```
public_key.pub
private_key.seal
```
or 
```
public_key_debug.pub
private_key_debug.seal
```

to convert this public key in a format usable by the 2nd app (`./encrypt`) run this : 
```sh
openssl rsa -RSAPublicKey_in -in public_key_debug.pub -inform DER -outform PEM -out pubkey.pem
```

now we can encrypt our data : 
```
./encrypt > example.enc
```
(`client_openssl.cpp` has the public key `pubkey.pem` & input data `example.bin` hardcoded)
We provide an example update in `example.bin`. This is a binary file containing all weights of network (in float32).
In our testing we used a this model (using tensorflow.keras) which is used to activity recognition using gyroscopic/accelerometic data : 
```python
model = Sequential()
model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(p_height, p_width,1)))
model.add(MaxPooling2D((2, 2)))
model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
model.add(MaxPooling2D((2, 2)))
model.add(Dense(1000, activation='relu', kernel_initializer='he_uniform'))#1000
model.add(Dense(500, activation='relu', kernel_initializer='he_uniform'))#500
model.add(Dense(500, activation='relu', kernel_initializer='he_uniform'))#100
model.add(Flatten())
    
model.add(Dense(4, activation='softmax'))
```

with a running server & a running SGX app we can now feed update to the enclave : 
```sh
ab -c 1 -n 20 -H "Accept-Encoding: gzip, deflate" -p ./example.enc http://localhost:8181/upload
```
this will send 20 times the encrypted update. with default configuration, this will result in 11 updates sent to the target server. 
