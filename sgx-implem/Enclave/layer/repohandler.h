#ifndef REPOS_HANDLER_H_
#define REPOS_HANDLER_H_
#include "repo.h"
#include "Layers.h"

class ReposHandler{
    private:
        Repo* repos[6];
        sgx_thread_mutex_t updateMutex;
    public:
        ReposHandler(int threshold);
        ~ReposHandler();
        bool addUpdateToRepos(Update *received, Update* out);
        void status(){
            printf("{ ");
            for(int i = 0; i < 4; i++){
                repos[i]->status();
            }
            printf(" }\n");
        }
};

#endif