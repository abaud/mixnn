#include "repo.h"

Conv1_Repo::Conv1_Repo(int threshold) : Repo(1, threshold){
    this->pool = new Layer_1[threshold*2];
}

Conv1_Repo::~Conv1_Repo(){
    delete[] this->pool;
}

void Conv1_Repo::addToRepoInternal(Layer *layer, int target){
    Layer_1 *actual = (Layer_1*)layer;
    memcpy(&this->pool[target], actual, sizeof(Layer_1));
}

bool Conv1_Repo::copyFromRepoInternal(Layer *dest, int src){

    memcpy(dest, &this->pool[src], sizeof(Layer_1));
    return true;
}