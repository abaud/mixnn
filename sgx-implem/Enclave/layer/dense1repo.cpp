#include "repo.h"

Dense1_Repo::Dense1_Repo(int threshold) : Repo(3,threshold){
    this->pool = new Layer_3[threshold*2];
}

Dense1_Repo::~Dense1_Repo(){
    delete[] this->pool;
}

void Dense1_Repo::addToRepoInternal(Layer *layer, int target){
    Layer_3 *actual = (Layer_3*)layer;
    memcpy(&this->pool[target], actual, sizeof(Layer_3));
}

bool Dense1_Repo::copyFromRepoInternal(Layer *dest, int src){
    memcpy(dest, &this->pool[src], sizeof(Layer_3));
    return true;
}