#include "repo.h"

Dense3_Repo::Dense3_Repo(int threshold) : Repo(5, threshold){
    this->pool = new Layer_5[threshold*2];
}

Dense3_Repo::~Dense3_Repo(){
    delete[] this->pool;
}

void Dense3_Repo::addToRepoInternal(Layer *layer, int target){
    Layer_5 *actual = (Layer_5*)layer;
    memcpy(&this->pool[target], actual, sizeof(Layer_5));
}

bool Dense3_Repo::copyFromRepoInternal(Layer *dest, int src){
    memcpy(dest, &this->pool[src], sizeof(Layer_5));
    return true;
}