#include "repo.h"

Dense2_Repo::Dense2_Repo(int threshold) : Repo(4, threshold){
    this->pool = new Layer_4[threshold*2];
}

Dense2_Repo::~Dense2_Repo(){
    delete[] this->pool;
}

void Dense2_Repo::addToRepoInternal(Layer *layer, int target){
    Layer_4 *actual = (Layer_4*)layer;
    memcpy(&this->pool[target], actual, sizeof(Layer_4));
}

bool Dense2_Repo::copyFromRepoInternal(Layer *dest, int src){
    memcpy(dest, &this->pool[src], sizeof(Layer_4));
    return true;
}