#include "updaterepo.h"
#include "rand.h"
#include <algorithm>

void UpdateRepo::addToRepo(Update *update){
    int target;
    this->sem->acquire();
    this->readlock();
    target = this->emptySpaces.front();
    this->emptySpaces.pop_front();
    this->readunlock();

    memcpy(&this->pool[target], update, sizeof(Update));

    this->writelock();
    this->filled.push_back(target);
    this->writeunlock();
}

bool UpdateRepo::getAverageUpdate(Update *dest){
    if(!this->canAverage()){
        return false;
    }

    this->avglock();
    if(!this->canAverage()){
        this->avgunlock();
        return false;
    }
    int target;
    for(int i = 0; i < threshold; i++){
        this->writelock();
        target = this->filled.front();
        this->filled.pop_front();
        this->writeunlock();
        *dest += this->pool[target];
        this->readlock();
        this->emptySpaces.push_back(target);
        this->readunlock();
        this->sem->release();
    }
    *dest /= threshold;
    this->avgunlock();
    return true;
}

bool UpdateRepo::getMedianUpdate(Update *dest){
    if(!this->canAverage() || this->isAveraging()){
        return false;
    }

    this->avglock();
    if(!this->canAverage()){
        this->avgunlock();
        return false;
    }   
    this->averaging = true;
    
    std::vector<int> list;
    std::vector<float> temp; 
    this->writelock();
    for(int i = 0; i < threshold; i++){
        list.push_back(this->filled.front());
        this->filled.pop_front();
    }
    this->writeunlock();

    int n = list.size()/2;

    // layer 1
    for(int i = 0; i < LAYER1SIZE; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer1.weights[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer1.weights[i] = temp[n];
    }

    for(int i = 0; i < LAYER1OUTPUT; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer1.output[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer1.output[i] = temp[n];
    }

    // layer 2
    for(int i = 0; i < LAYER2SIZE; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer2.weights[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer2.weights[i] = temp[n];
    }

    for(int i = 0; i < LAYER2OUTPUT; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer2.output[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer2.output[i] = temp[n];
    }

    // layer 3
    for(int i = 0; i < LAYER3SIZE; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer3.weights[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer3.weights[i] = temp[n];
    }

    for(int i = 0; i < LAYER3OUTPUT; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer3.output[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer3.output[i] = temp[n];
    }
    
    //layer 4
    for(int i = 0; i < LAYER4SIZE; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer4.weights[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer4.weights[i] = temp[n];
    }

    for(int i = 0; i < LAYER4OUTPUT; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer4.output[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer4.output[i] = temp[n];
    }

    //layer 5
    for(int i = 0; i < LAYER5SIZE; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer5.weights[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer5.weights[i] = temp[n];
    }

    for(int i = 0; i < LAYER5OUTPUT; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer5.output[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer5.output[i] = temp[n];
    }

    //layer 6
    for(int i = 0; i < LAYER6SIZE; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer6.weights[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer6.weights[i] = temp[n];
    }

    for(int i = 0; i < LAYER6OUTPUT; i++){
        for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
            temp.push_back(this->pool[*it].layer6.output[i]);
        }
        std::nth_element(temp.begin(), temp.begin()+n, temp.end());
        dest->layer6.output[i] = temp[n];
    }

    this->readlock();
    for(std::vector<int>::iterator it = list.begin(); it != list.end(); it++){
        this->emptySpaces.push_back(*it);
        this->sem->release();
    } 
    this->readunlock();

    this->averaging = false;
    this->avgunlock();
    return true;
}