#ifndef LAYERS_H_
#define LAYERS_H_

#include <stdint.h>
#include <sgx_thread.h>

#define MAX_ITEMS 10
#define UPLOAD_THRESHOLD 5
typedef struct Layer {
    Layer& operator+=(const Layer& rhs){ return *this;};
} Layer;
/**
 * new layer structs below 
 */

#define LAYER1SIZE 3*3*1*32
#define LAYER1OUTPUT 32
typedef struct Layer_1 : Layer {
    float weights[LAYER1SIZE];
    float output[LAYER1OUTPUT];

    Layer_1& operator+=(const Layer_1& rhs){
        for(int i = 0; i < LAYER1SIZE; i++){
            weights[i] += rhs.weights[i];
        }
        for(int i = 0; i < LAYER1OUTPUT; i++){
            output[i] += rhs.output[i];
        }
        return *this;
    }

    Layer_1& operator/=(const int rhs){
        for(int i = 0; i < LAYER1SIZE; i++){
            weights[i] /= rhs;
        }
        for(int i = 0; i < LAYER1OUTPUT; i++){
            output[i] /= rhs;
        }
        return *this;
    }

} Layer_1;

#define LAYER2SIZE 3*3*32*32
#define LAYER2OUTPUT 32
typedef struct Layer_2 : Layer {
    float weights[LAYER2SIZE];
    float output[LAYER2OUTPUT];

    Layer_2& operator+=(const Layer_2& rhs){
        for(int i = 0; i < LAYER2SIZE; i++){
            weights[i] += rhs.weights[i];
        }
        for(int i = 0; i < LAYER2OUTPUT; i++){
            output[i] += rhs.output[i];
        }
        return *this;
    }

    Layer_2& operator/=(const int rhs){
        for(int i = 0; i < LAYER2SIZE; i++){
            weights[i] /= rhs;
        }
        for(int i = 0; i < LAYER2OUTPUT; i++){
            output[i] /= rhs;
        }
        return *this;
    }
} Layer_2;

#define LAYER3SIZE 32*1000
#define LAYER3OUTPUT 1000
typedef struct Layer_3 : Layer {
    float weights[LAYER3SIZE];
    float output[LAYER3OUTPUT];

    Layer_3& operator+=(const Layer_3& rhs){
        for(int i = 0; i < LAYER3SIZE; i++){
            weights[i] += rhs.weights[i];
        }
        for(int i = 0; i < LAYER3OUTPUT; i++){
            output[i] += rhs.output[i];
        }
        return *this;
    }

    Layer_3& operator/=(const int rhs){
        for(int i = 0; i < LAYER3SIZE; i++){
            weights[i] /= rhs;
        }
        for(int i = 0; i < LAYER3OUTPUT; i++){
            output[i] /= rhs;
        }
        return *this;
    }

} Layer_3;

#define LAYER4SIZE 1000*500
#define LAYER4OUTPUT 500
typedef struct Layer_4 : Layer {
    float weights[LAYER4SIZE];
    float output[LAYER4OUTPUT];

    Layer_4& operator+=(const Layer_4& rhs){
        for(int i = 0; i < LAYER4SIZE; i++){
            weights[i] += rhs.weights[i];
        }
        for(int i = 0; i < LAYER4OUTPUT; i++){
            output[i] += rhs.output[i];
        }
        return *this;
    }

    Layer_4& operator/=(const int rhs){
        for(int i = 0; i < LAYER4SIZE; i++){
            weights[i] /= rhs;
        }
        for(int i = 0; i < LAYER4OUTPUT; i++){
            output[i] /= rhs;
        }
        return *this;
    }
} Layer_4;

#define LAYER5SIZE 500*500
#define LAYER5OUTPUT 500
typedef struct Layer_5 : Layer {
    float weights[LAYER5SIZE];
    float output[LAYER5OUTPUT];

    Layer_5& operator+=(const Layer_5& rhs){
        for(int i = 0; i < LAYER5SIZE; i++){
            weights[i] += rhs.weights[i];
        }
        for(int i = 0; i < LAYER5OUTPUT; i++){
            output[i] += rhs.output[i];
        }
        return *this;
    }

    Layer_5& operator/=(const int rhs){
        for(int i = 0; i < LAYER5SIZE; i++){
            weights[i] /= rhs;
        }
        for(int i = 0; i < LAYER5OUTPUT; i++){
            output[i] /= rhs;
        }
        return *this;
    }
} Layer_5;

#define LAYER6SIZE 6000*4
#define LAYER6OUTPUT 4
typedef struct Layer_6 : Layer {
    float weights[LAYER6SIZE];
    float output[LAYER6OUTPUT];

    Layer_6& operator+=(const Layer_6& rhs){
        for(int i = 0; i < LAYER6SIZE; i++){
            weights[i] += rhs.weights[i];
        }
        for(int i = 0; i < LAYER6OUTPUT; i++){
            output[i] += rhs.output[i];
        }
        return *this;
    }

    Layer_6& operator/=(const int rhs){
        for(int i = 0; i < LAYER6SIZE; i++){
            weights[i] /= rhs;
        }
        for(int i = 0; i < LAYER6OUTPUT; i++){
            output[i] /= rhs;
        }
        return *this;
    }
} Layer_6;

// layers
typedef struct Update {
    Layer_1 layer1;
    Layer_2 layer2;
    Layer_3 layer3;
    Layer_4 layer4;
    Layer_5 layer5;
    Layer_6 layer6;

    Update& operator+=(const Update& rhs){
        layer1 += rhs.layer1;
        layer2 += rhs.layer2;
        layer3 += rhs.layer3;
        layer4 += rhs.layer4;
        layer5 += rhs.layer5;
        layer6 += rhs.layer6;
        return *this;
    }

    Update& operator/=(const int rhs){
        layer1 /= rhs;
        layer2 /= rhs;
        layer3 /= rhs;
        layer4 /= rhs;
        layer5 /= rhs;
        layer6 /= rhs;
        return *this;
    }
} Update;

#endif  // LAYERS_H_