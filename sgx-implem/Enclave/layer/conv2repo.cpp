#include "repo.h"

Conv2_Repo::Conv2_Repo(int threshold) : Repo(2, threshold){
    this->pool = new Layer_2[threshold*2];
}

Conv2_Repo::~Conv2_Repo(){
    delete[] this->pool;
}

void Conv2_Repo::addToRepoInternal(Layer *layer, int target){
    Layer_2 *actual = (Layer_2*)layer;
    memcpy(&this->pool[target], actual, sizeof(Layer_2));
}

bool Conv2_Repo::copyFromRepoInternal(Layer *dest, int src){
    memcpy(dest, &this->pool[src], sizeof(Layer_2));
    return true;
}