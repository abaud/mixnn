#include "repohandler.h"

ReposHandler::ReposHandler(int threshold){
    this->repos[0] = new Conv1_Repo(threshold);
    this->repos[1] = new Conv2_Repo(threshold);
    this->repos[2] = new Dense1_Repo(threshold); 
    this->repos[3] = new Dense2_Repo(threshold);
    this->repos[4] = new Dense3_Repo(threshold);
    this->repos[5] = new Dense4_Repo(threshold);
    this->updateMutex = SGX_THREAD_MUTEX_INITIALIZER;
}

ReposHandler::~ReposHandler(){
    for(int i; i < 6; i++){
        delete this->repos[i];
    }
    sgx_thread_mutex_destroy(&this->updateMutex);
}

bool ReposHandler::addUpdateToRepos(Update *received, Update *out){
    bool hasUpdate = false;
    
    this->repos[0]->addToRepo(&received->layer1);
    this->repos[1]->addToRepo(&received->layer2);
    this->repos[2]->addToRepo(&received->layer3);
    this->repos[3]->addToRepo(&received->layer4);
    this->repos[4]->addToRepo(&received->layer5);
    this->repos[5]->addToRepo(&received->layer6);

    sgx_thread_mutex_lock(&this->updateMutex);
    if(this->repos[0]->shouldUpdate() && this->repos[1]->shouldUpdate() && \
        this->repos[2]->shouldUpdate() && this->repos[3]->shouldUpdate() && \
        this->repos[4]->shouldUpdate() && this->repos[5]->shouldUpdate()){
        hasUpdate = this->repos[0]->copyFromRepo(&out->layer1);
        hasUpdate &= this->repos[1]->copyFromRepo(&out->layer2);
        hasUpdate &= this->repos[2]->copyFromRepo(&out->layer3);
        hasUpdate &= this->repos[3]->copyFromRepo(&out->layer4);
        hasUpdate &= this->repos[4]->copyFromRepo(&out->layer5);
        hasUpdate &= this->repos[5]->copyFromRepo(&out->layer6);
    }
    sgx_thread_mutex_unlock(&this->updateMutex);
    //this->status();
    return hasUpdate;
}
