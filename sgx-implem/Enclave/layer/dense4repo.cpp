#include "repo.h"

Dense4_Repo::Dense4_Repo(int threshold) : Repo(6, threshold){
    this->pool = new Layer_6[threshold*2];
}

Dense4_Repo::~Dense4_Repo(){
    delete[] this->pool;
}

void Dense4_Repo::addToRepoInternal(Layer *layer, int target){
    Layer_6 *actual = (Layer_6*)layer;
    memcpy(&this->pool[target], actual, sizeof(Layer_6));
}

bool Dense4_Repo::copyFromRepoInternal(Layer *dest, int src){
    memcpy(dest, &this->pool[src], sizeof(Layer_6));
    return true;
}