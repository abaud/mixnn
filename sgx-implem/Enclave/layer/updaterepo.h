#ifndef UPDATE_REPO_H_
#define UPDATE_REPO_H_

#include <rand.h>
#include <sgx_semaphore.h>
#include <list>
#include <vector>
#include <string.h>
#include <sgx_thread.h>
#include "Enclave_t.h"
#include "Layers.h"
#include "utils/log.h"


class UpdateRepo {
    private:
        Update *pool;
        std::list<int> filled;
        std::list<int> emptySpaces;
        Semaphore *sem;
        sgx_thread_mutex_t readMutex;
        sgx_thread_mutex_t writeMutex;
        sgx_thread_mutex_t avgMutex;
        int threshold;
        bool averaging;

        bool isAveraging(){ return averaging;}

        //mutex
        void readlock(){
            sgx_thread_mutex_lock(&this->readMutex);
        };
        void readunlock(){
            sgx_thread_mutex_unlock(&this->readMutex);
        };

        void writelock(){
            sgx_thread_mutex_lock(&this->writeMutex);
        };
        void writeunlock(){
            sgx_thread_mutex_unlock(&this->writeMutex);
        };

        void avglock(){
            sgx_thread_mutex_lock(&this->avgMutex);

        };
        void avgunlock(){
            sgx_thread_mutex_unlock(&this->avgMutex);
        };
    public:
        UpdateRepo(int threshold){
            int maxitems = threshold*2;
            this->sem = new Semaphore(maxitems);
            this->threshold = threshold;
            this->filled = {};
            this->emptySpaces = {};
            for(int i = 0; i < maxitems; i++){
                this->emptySpaces.push_back(i);
            }
            this->readMutex = SGX_THREAD_MUTEX_INITIALIZER;
            this->writeMutex = SGX_THREAD_MUTEX_INITIALIZER;
            this->avgMutex = SGX_THREAD_MUTEX_INITIALIZER;
            this->pool = new Update[maxitems];
        };
        ~UpdateRepo(){
            delete sem;
            sgx_thread_mutex_destroy(&this->readMutex);
            sgx_thread_mutex_destroy(&this->writeMutex);
            sgx_thread_mutex_destroy(&this->avgMutex);
            delete[] pool;
        }
        void addToRepo(Update *update);
        bool canAverage(){ return (this->filled.size() >= threshold);};
        bool getAverageUpdate(Update *dest);
        bool getMedianUpdate(Update *dest);
};

#endif