#ifndef REPO_H_
#define REPO_H_

#include <rand.h>
#include <sgx_semaphore.h>
#include <list>
#include <vector>
#include <string.h>
#include <sgx_thread.h>
#include "Enclave_t.h"
#include "Layers.h"
#include "utils/log.h"


class Repo {
    protected:
        int id;
        Layer *pool;
        std::vector<int> filled;
        std::list<int> emptySpaces;
        Semaphore *sem;
        sgx_thread_mutex_t readMutex;
        sgx_thread_mutex_t writeMutex;
        int threshold;
        virtual void addToRepoInternal(Layer *layer, int target) = 0;
        virtual bool copyFromRepoInternal(Layer *dest, int src) = 0;
    public:
        Repo(int id, int threshold){
            int maxitems = threshold*2;
            this->sem = new Semaphore(maxitems);
            this->threshold = threshold;
            printf("MAX ITEMS = %d THRESHOLD %d \n", maxitems, threshold);
            this->filled = {};
            this->emptySpaces = {};
            for(int i = 0; i < maxitems; i++){
                this->emptySpaces.push_back(i);
            }
            this->id = id;
            this->readMutex = SGX_THREAD_MUTEX_INITIALIZER;
            this->writeMutex = SGX_THREAD_MUTEX_INITIALIZER;
        };
        ~Repo(){
            delete sem;
            sgx_thread_mutex_destroy(&this->readMutex);
            sgx_thread_mutex_destroy(&this->writeMutex);
        };
        bool shouldUpdate(){ return (this->filled.size() >= threshold);};
        //mutex
        void readlock(){
            sgx_thread_mutex_lock(&this->readMutex);
        };
        void readunlock(){
            sgx_thread_mutex_unlock(&this->readMutex);
        };
        void writelock(){
            sgx_thread_mutex_lock(&this->writeMutex);
        };
        void writeunlock(){
            sgx_thread_mutex_unlock(&this->writeMutex);
        };
        //*/
        void addToRepo(Layer *layer){
            int target;
            this->sem->acquire();
            this->readlock();
            target = this->emptySpaces.front();
            this->emptySpaces.pop_front();
            this->readunlock();
            this->addToRepoInternal(layer, target);
            this->writelock();
            this->filled.push_back(target);
            this->writeunlock();
        }
        bool copyFromRepo(Layer *dest){
            bool ret;
            int rand;
            this->writelock();
            if(!this->shouldUpdate()){
                this->writeunlock();
                return false;
            }
            rand = random_int(0, this->filled.size() - 1);
            int src = this->filled[rand];
            if (rand != this->filled.size() - 1){
                this->filled[rand] = std::move(this->filled.back());
            }
            this->filled.pop_back();
            this->writeunlock();
            ret = this->copyFromRepoInternal(dest, src);
            this->readlock();
            this->emptySpaces.push_back(src);
            this->readunlock();
            this->sem->release();
            return ret;
        }

        void status(){
            printf("{%d : %d/%d}", this->id, this->emptySpaces.size(), this->filled.size());
        }

};

class Conv1_Repo : public Repo {
    protected:
        void addToRepoInternal(Layer *layer, int target);
        bool copyFromRepoInternal(Layer *dest, int src);
    public:
        Conv1_Repo(int maxitems);
        ~Conv1_Repo();
};

class Conv2_Repo : public Repo {
    protected:
        void addToRepoInternal(Layer *layer, int target);
        bool copyFromRepoInternal(Layer *dest, int src);
    public:
        Conv2_Repo(int maxitems);
        ~Conv2_Repo(); 
};

class Dense1_Repo : public Repo {
    protected:
        void addToRepoInternal(Layer *layer, int target);
        bool copyFromRepoInternal(Layer *dest, int src);
    public:
        Dense1_Repo(int maxitems);
        ~Dense1_Repo();
};

class Dense2_Repo : public Repo {
    protected: 
        void addToRepoInternal(Layer *layer, int target);
        bool copyFromRepoInternal(Layer *dest, int src);
    public:
        Dense2_Repo(int maxitems);
        ~Dense2_Repo();
};

class Dense3_Repo : public Repo {
    protected: 
        void addToRepoInternal(Layer *layer, int target);
        bool copyFromRepoInternal(Layer *dest, int src);
    public:
        Dense3_Repo(int maxitems);
        ~Dense3_Repo();
};

class Dense4_Repo : public Repo {
    protected: 
        void addToRepoInternal(Layer *layer, int target);
        bool copyFromRepoInternal(Layer *dest, int src);
    public:
        Dense4_Repo(int maxitems);
        ~Dense4_Repo();
};

#endif