#include "Enclave.h"

#include <assert.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>
#include "Enclave_t.h"
#include "crypto/crypto.h"
#include "sgx_trts.h"
#include "sgx_tseal.h"
#include "sgx_thread.h"
#include "layer/repohandler.h"
#include "layer/updaterepo.h"


ReposHandler* reposHandler;
UpdateRepo* updateRepo;
unsigned char *pkey;


void ecall_init(int threshold, int useRepoHandler, int useUpdateRepo){
    if(useRepoHandler)
        reposHandler = new ReposHandler(threshold);
    if(useUpdateRepo)
        updateRepo = new UpdateRepo(threshold);
    load_private_key(&pkey);
}

void add_bin_message(uint8_t* message){
    Update *received_update = (Update *)message;
    Update *update = (Update*)malloc(sizeof(Update));
    bool updated = reposHandler->addUpdateToRepos(received_update, update);
    if(updated){
        save_update_layer((uint8_t*)update, sizeof(Update));
    }
    free(update);
}

void decryptAndAdd(char *encrypted) {
    char *message;
    EVP_PKEY *evp_pkey;
    printf("evp creation.\n");
    create_evp_pkey(&evp_pkey,pkey);
    printf("evp created.\n");
    decrypt(encrypted, &message, evp_pkey);
    printf("decrypted.\n");
    add_bin_message((uint8_t*)message);
    printf("message added.\n");
    free(message);
}

void decryptOnly(char *encrypted) {
    char *message;
    EVP_PKEY *evp_pkey;
    create_evp_pkey(&evp_pkey,pkey);
    decrypt(encrypted, &message, evp_pkey);
    save_update_layer((uint8_t*)message, sizeof(Update));
    free(message);
}

void median(uint8_t* message){
    Update *received_update = (Update *)message;
    Update *update = (Update*)malloc(sizeof(Update));
    updateRepo->addToRepo(received_update);
    if(updateRepo->getMedianUpdate(update)){
        save_update_layer((uint8_t*)update, sizeof(Update));
    }
    free(update);
}
void decryptAndMedian(char *encrypted) {
    char *message;
    EVP_PKEY *evp_pkey;
    create_evp_pkey(&evp_pkey,pkey);
    decrypt(encrypted, &message, evp_pkey);
    median((uint8_t*)message);
    free(message);
}


void average(uint8_t* message){
    Update *received_update = (Update *)message;
    Update *update = (Update*)malloc(sizeof(Update));
    updateRepo->addToRepo(received_update);
    if(updateRepo->getAverageUpdate(update)){
        save_update_layer((uint8_t*)update, sizeof(Update));
    }
    free(update);
}
void decryptAndAvg(char *encrypted) {
    char *message;
    EVP_PKEY *evp_pkey;
    create_evp_pkey(&evp_pkey,pkey);
    decrypt(encrypted, &message, evp_pkey);
    average((uint8_t*)message);
    free(message);
}


void ecall_destroy(){
    delete reposHandler;
    delete updateRepo;
    free(pkey);
}
