#ifndef ENCLAVE_CRYPTO_H_
#define ENCLAVE_CRPYTO_H_
#include <openssl/evp.h>

#define SEALED_PRIVATE_SIZE 2909
#define PRIVATE_KEY_SIZE 2351
#define RSA_KEY_SIZE 4096

void create_evp_pkey(EVP_PKEY **evp_pkey, const unsigned char *pkey);
void load_private_key(unsigned char **pkey);
void decrypt(char* encrypted, char **decrypted, EVP_PKEY *evp_pkey);

#endif