#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/pem.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include <fstream>
#include <iostream>

#include "base64.h"

using namespace std;

static int envelope_seal(EVP_PKEY *pub_key, unsigned char *plaintext,
                         int plaintext_len, unsigned char **encrypted_key,
                         int *encrypted_key_len, unsigned char **iv,
                         int *iv_len,  unsigned char **ciphertext,
                         int *ciphertext_len, unsigned char *tag)
{
    EVP_CIPHER_CTX *ctx;
    int len, ret = 0;
    const EVP_CIPHER *type = EVP_aes_256_gcm();
    unsigned char *tmpiv = NULL, *tmpenc_key = NULL, *tmpctxt = NULL;

    if((ctx = EVP_CIPHER_CTX_new()) == NULL)
        return 0;
    *iv_len = EVP_CIPHER_iv_length(type);
    if ((tmpiv = (unsigned char*)malloc(*iv_len)) == NULL)
        goto err;
    if ((tmpenc_key = (unsigned char*)malloc(EVP_PKEY_size(pub_key))) == NULL)
        goto err;
    if ((tmpctxt = (unsigned char*)malloc(plaintext_len + EVP_CIPHER_block_size(type)))
            == NULL)
        goto err;
    if(EVP_SealInit(ctx, type, &tmpenc_key, encrypted_key_len, tmpiv, &pub_key,
                    1) != 1)
        goto err;

    if(EVP_SealUpdate(ctx, tmpctxt, &len, plaintext, plaintext_len) != 1)
        goto err;
    *ciphertext_len = len;

    if(EVP_SealFinal(ctx, tmpctxt + len, &len) != 1)
        goto err;
    *ciphertext_len += len;
    *iv = tmpiv;
    *encrypted_key = tmpenc_key;
    *ciphertext = tmpctxt;

    /* Get the tag */
    if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag))
        goto err;

    tmpiv = NULL;
    tmpenc_key = NULL;
    tmpctxt = NULL;
    ret = 1;
 err:
    EVP_CIPHER_CTX_free(ctx);
    free(tmpiv);
    free(tmpenc_key);
    free(tmpctxt);

    return ret;
}


int main(void)
{
    EVP_PKEY *pubkey = NULL;
    FILE *pubkeyfile;
    int ret = 1;
    unsigned char  *iv = NULL, *tag = NULL, *enc_key = NULL, *ciphertext = NULL, *plaintext = NULL;
    char *message = NULL, *b64iv = NULL, *b64enc_key = NULL, *b64ciphertext = NULL, *b64tag = NULL;
    int iv_len = 0, enc_key_len = 0, ciphertext_len = 0, plaintext_len = 0;
    long fsize;
    struct stat st;

    if(stat("example.bin", &st)){
        printf("example.bin doesn't exist.");
        return ret;
    }

    fsize = st.st_size;
    message = (char*)malloc(fsize);
    ifstream file("example.bin", ios::in | ios::binary);
    file.read((char *)message, fsize);
    file.close();
    
    if ((pubkeyfile = fopen("pubkey.pem", "r")) == NULL) {
        printf("Failed to open public key for reading\n");
        goto err;
    }

    if ((pubkey = PEM_read_PUBKEY(pubkeyfile, &pubkey, NULL, NULL)) == NULL) {
        fclose(pubkeyfile);
        goto err;
    }

    fclose(pubkeyfile);
    tag = (unsigned char*)malloc(16);
    if (!envelope_seal(pubkey, (unsigned char*)message, fsize, &enc_key, &enc_key_len,
                       &iv, &iv_len, &ciphertext, &ciphertext_len, tag))
        goto err;
    b64iv = base64::base64(iv,iv_len, NULL);
    printf("%s", b64iv);
    printf("\n");
    b64enc_key = base64::base64(enc_key, enc_key_len, NULL);
    printf("%s", b64enc_key);
    printf("\n");
    b64ciphertext = base64::base64(ciphertext, ciphertext_len, NULL);
    printf("%s", b64ciphertext);
    printf("\n");
    b64tag = base64::base64(tag, 16, NULL);
    printf("%s", b64tag);
    printf("\n");
    ret = 0;
 
 err:
    if (ret != 0) {
        printf("Error\n");
        ERR_print_errors_fp(stdout);
    }
    EVP_PKEY_free(pubkey);
    free(iv);
    free(enc_key);
    free(ciphertext);
    free(plaintext);

    return ret;
}