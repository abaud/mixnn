from Method import LinkabilityAttackMixnet
from setup import setup, protocole
import pickle
from pathlib import Path

# Method = "FedAvg"#"FedAvg","FedNoises","FedMed","FedPruning16bit"
# Problem = "LFW"#"Cifar10","MotionSense","MobiAct","LFW"
# batch_CV = "b0"#"b0","b1","b2"
try:
    import sys
    Method, Problem, batch_CV = sys.argv[1], sys.argv[2], sys.argv[3]
    save=True
except:
    Method = "FedAvg"#"FedAvg","FedNoises","FedMed","FedPruning16bit"
    Problem = "MotionSense"#"Cifar10","MotionSense","MobiAct","LFW"
    batch_CV = "b0"#"b0","b1","b2"
    save=False

config = setup(Problem)
config_method = protocole(Method)
config["hparams"]["N"]+=1
Federated = LinkabilityAttackMixnet(config, config_method)

Federated.compute(10)
config["hparams"]["N"]+=-1
model_name = "{}_Link_E{}_B{}_N{}_K{}.sav".format(Problem+"Mix"+Method+batch_CV,config["hparams"]["E"], config["hparams"]["B"], config["hparams"]["N"], config["hparams"]["K"])

if save:
    pickle.dump(Federated.acc_attack, open(Path("AccLink","0Mean_"+model_name), 'wb'))
    pickle.dump(Federated.perf_client, open(Path("AccLink","1CDF_"+model_name), 'wb'))
