from Method import ProgressiveMixnet
from setup import setup, protocole
import pickle
from pathlib import Path
from copy import deepcopy
import sys
try :
    Method, Problem, batch_CV = sys.argv[1], sys.argv[2], sys.argv[3]
    save = True
except:
    Method = "FedAvg"#"FedAvg","FedNoises","FedMed","FedPruning16bit"
    Problem = "Cifar10"#"Cifar10","MotionSense","MobiAct","LFW"
    batch_CV = "b0"#"b0","b1","b2"
    save = False
config = setup(Problem)
config_method = protocole(Method)
K = deepcopy(config['hparams']["K"])
# if save==False:
config['hparams']["K"] = int(K//5)
    # config['hparams']["N"] = 5
    # config['hparams']["E"] = 1
    # config['hparams']["B"] = 128
save = True
Federated = ProgressiveMixnet(config, config_method, int(K//4))
Federated.compute()

model_name = "{}_ProgessiveMixnet_E{}_B{}_N{}_K{}.json".format(Problem+Method+batch_CV,config["hparams"]["E"], config["hparams"]["B"], config["hparams"]["N"], config["hparams"]["K"])
Federated.plot_model(Problem+"_PMixnet.png")

if save:
    pickle.dump(Federated.Accuracy, open(Path("AccMixnet","0Van_"+model_name), 'wb'))
    pickle.dump(Federated.MixAccuracy, open(Path("AccMixnet","1Mix_"+model_name), 'wb'))


