from tensorflow.keras.utils import to_categorical
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from random import sample
import numpy as np
from copy import deepcopy
from pickle import dump, load
from pathlib import Path
import random
import matplotlib.pyplot as plt
import tensorflow as tf
# from PlayGroundOrdering import CanonicalRustine


def weight_to_array(weights):
    return np.concatenate([lay.flatten() for lay in weights])


class FederatedMixnet():
    def __init__(self, setup_dict, protocole_dict, list_cli=None):
        self.protocole = protocole_dict["method"]
        self.protocole.__init__(self, setup_dict, list_cli=list_cli)
        self.MixAccuracy = {client: [0 for i in range(
            self.hparams['N'])] for client in self.list_client}
        self.mix_weights = self.model.get_weights()
        self.dp_side = setup_dict['dp_side']
        self.noise_multiplier = setup_dict['noise_multiplier']
        self.clip_norm = setup_dict['clip_norm']

    def MixUpdates(self, Updates, get_key=False):
        # Federated Learning : Mixnet side
        selected_clients = list(Updates.keys())

        Updates_Mix = dict()

        print("Entering Mixnet")

        n_tot = sum([len(self.data[str(id_client)]['x_train']) for id_client in selected_clients])
        Mixnet_key = np.array([list(map(int, random.sample(range(len(selected_clients)), len(
            selected_clients)))) for i in range(len(self.agg_weights))])

        for i, id_client in enumerate(selected_clients):
            Updates_Mix[id_client] = deepcopy(Updates[id_client])

            for num_layer in range(len(self.agg_weights)):
                update = Updates[str(selected_clients[int(Mixnet_key[num_layer][i])])][num_layer]
                if self.dp_side == 'mixnet':
                    clip_norm = self.clip_norm
                    noise_multiplier = self.noise_multiplier
                    update = tf.clip_by_norm(update, clip_norm)
                    update = update + \
                        np.random.normal(0, clip_norm**2*noise_multiplier, update.shape)
                Updates_Mix[id_client][num_layer] = (
                    len(self.data[str(id_client)]['x_train'])/n_tot) * update

        print("Exiting Mixnet")
        if get_key:
            return Updates_Mix, Mixnet_key
        else:
            return Updates_Mix

    def aggregate_mix_updates(self, Update):
        nb_layers = len(self.mix_weights)
        selected_clients = list(Update.keys())
        n_tot = sum([len(self.data[str(id_client)]['x_train']) for id_client in selected_clients])
        out_weights = deepcopy(self.mix_weights)
        for n_layer in range(nb_layers):
            layer = self.mix_weights[n_layer]

            for id_client in selected_clients:
                layer += (len(self.data[str(id_client)]['x_train'])/n_tot) * \
                    Update[str(id_client)][n_layer]

            out_weights[n_layer] = layer
        return out_weights

    def evaluate_mixnet(self, i):
        self.model.set_weights(self.mix_weights)
        for id_client in self.list_client:
            _, acc_client = self.model.evaluate(
                self.data[str(id_client)]['x_test'], self.data[str(id_client)]['y_test'])
            self.MixAccuracy[id_client][i] = acc_client
            print("Aggregated model precision, Client {} : {}\n".format(id_client, acc_client))

    def compute(self, init_weight=None, list_cli=None):

        if init_weight == None:
            self.agg_weights = self.ini_weights
        else:
            self.agg_weights = init_weight

        if list_cli == None:
            self.list_client = list(self.data.keys())
        else:
            self.list_client = list_cli
        K = min(self.hparams['K'], len(self.list_client))
        # Learning Rounds
        for i_round, num_round in enumerate(range(1, self.hparams['N']+1)):
            print("\n\n######################################################################\n################## Learning round {} ################################\n######################################################################\n\n".format(
                ("000"+str(num_round))[-3:]))

            selected_clients = sample(self.list_client, K)

            # Federated Learning : client side
            Updates = {id_client: self.protocole.update_client(
                self, id_client, self.agg_weights) for id_client in selected_clients}

            # Mixnet Side
            Update_Mix = self.MixUpdates(Updates)

            # Federated Learning : Server side
            self.agg_weights = self.protocole.aggregate_updates(self, Updates)
            self.mix_weights = self.protocole.aggregate_updates(self, Update_Mix)

            # Federated Learning : Aggregated model evaluation
            self.protocole.evaluate_model(self, i_round)
            self.evaluate_mixnet(i_round)

    def plot_model(self, name_image):  # reprendre
        list_rounds = list(range(len(self.Accuracy)))

        plt.plot(list_rounds, self.Accuracy, color="red", label="Classic")
        plt.plot(list_rounds, self.MixAccuracy["mix"], color="blue", label="MixNN", ls=":")

        plt.xlabel("Learning rounds", fontsize=15)
        plt.ylabel("Accuracy", fontsize=15)
        plt.grid(True)
        plt.savefig(name_image)
        plt.show()


class ProgressiveMixnet():
    def __init__(self, setup_dict, protocole_dict, size_enclave, proba=1, list_cli=None):
        self.protocole = protocole_dict["method"]
        self.protocole.__init__(self, setup_dict, list_cli=list_cli)
        self.MixAccuracy = {client: [0 for i in range(
            self.hparams['N'])] for client in self.list_client}
        self.mix_weights = self.model.get_weights()
        self.size_enclave = size_enclave
        self.proba = proba

    def aggregate_mix_updates(self, Update):
        nb_layers = len(self.mix_weights)
        selected_clients = list(Update.keys())
        n_tot = sum([len(self.data[str(id_client)]['x_train']) for id_client in selected_clients])
        out_weights = deepcopy(self.mix_weights)
        for n_layer in range(nb_layers):
            layer = self.mix_weights[n_layer]

            for id_client in selected_clients:
                layer += (len(self.data[str(id_client)]['x_train'])/n_tot) * \
                    Update[str(id_client)][n_layer]

            out_weights[n_layer] = layer
        return out_weights

    def evaluate_mixnet(self, i):
        self.model.set_weights(self.mix_weights)
        for id_client in self.list_client:
            _, acc_client = self.model.evaluate(
                self.data[str(id_client)]['x_test'], self.data[str(id_client)]['y_test'])
            self.MixAccuracy[id_client][i] = acc_client
            print("Aggregated model precision, Client {} : {}\n".format(id_client, acc_client))

    def update_mix_client(self, id_client, agg_weights):
        if np.random.rand() < self.proba:
            update = self.protocole.update_client(self, id_client, agg_weights)
            for i in range(len(self.agg_weights)):
                self.enclave[i].append(update[i])
            if len(self.enclave[0]) >= self.size_enclave:
                self.Updates[id_client] = list()
                select_vector = np.random.randint(0, self.size_enclave, len(self.agg_weights))
                for i in range(len(select_vector)):
                    self.Updates[id_client].append(self.enclave[i].pop(select_vector[i]))

    def compute(self, init_weight=None, list_cli=None):

        if init_weight == None:
            self.agg_weights = deepcopy(self.ini_weights)
            self.mix_weights = deepcopy(self.ini_weights)
        else:
            self.agg_weights = deepcopy(init_weight)
            self.mix_weights = deepcopy(init_weight)

        if list_cli == None:
            self.list_client = list(self.data.keys())
        else:
            self.list_client = list_cli

        K = min(self.hparams['K'], len(self.list_client))
        self.enclave = [list() for i in range(len(self.agg_weights))]
        # Learning Rounds
        for i_round, num_round in enumerate(range(1, self.hparams['N']+1)):
            print("\n\n######################################################################\n################## Learning round {} ################################\n######################################################################\n\n".format(
                ("000"+str(num_round))[-3:]))
            self.Updates = dict()

            selected_clients = sample(self.list_client, K)

            # Federated Learning : client side
            for id_client in selected_clients:
                self.update_mix_client(id_client, self.mix_weights)
            Updates_van = {id_client: self.protocole.update_client(
                self, id_client, self.agg_weights) for id_client in selected_clients}
            # Federated Learning : Server side
            if len(self.Updates) > 0:
                self.mix_weights = self.aggregate_mix_updates(self.Updates)
            self.agg_weights = self.protocole.aggregate_updates(self, Updates_van)

            # Federated Learning : Aggregated model evaluation
            self.protocole.evaluate_model(self, i_round)
            self.evaluate_mixnet(i_round)

    def plot_model(self, name_image):
        list_cli = list(self.Accuracy.keys())
        list_rounds = list(range(len(self.Accuracy[list_cli[0]])))
        list_acc = [np.mean(np.array([self.Accuracy[client][n_round]
                            for client in list_cli])) for n_round in list_rounds]
        list_mixacc = [np.mean(np.array([self.MixAccuracy[client][n_round]
                               for client in list_cli])) for n_round in list_rounds]
        plt.plot(list_rounds, list_acc, color="red", label="FedAvg")
        plt.plot(list_rounds, list_mixacc, color="blue", label="MixNN", ls=":")

        plt.xlabel("Learning rounds", fontsize=15)
        plt.ylabel("Accuracy", fontsize=15)
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.grid(True)
        plt.savefig(name_image)
        plt.show()


def last_2lines(weights):
    return weight_to_array([weights[i] for i in [-4, -2]])


def Canonical2(weight, num_round, id_cli, size=20):
    return np.concatenate([np.array([int(id_cli), num_round]), np.vstack(
        [np.mean(weight[-2], axis=1)] +
        [np.var(weight[-2], axis=1)] +
        [np.quantile(weight[-2], np.array(range(0, size+1, 1))/size, axis=1)]
    ).flatten()])


def Canonical(weight, flat_form=True, size=20):
    return(np.vstack(
        [np.mean(weight[-2], axis=1)] +
        [np.var(weight[-2], axis=1)] +
        [np.quantile(weight[-2], np.array(range(0, size+1, 1))/size, axis=1)]
    ).flatten())
    F_out = list()
    Weights = [{"weights": np.array(weight[i*2]), "bias": np.array(weight[i*2+1])}
               for i in range(len(weight)//2)]
    for t, layer in enumerate(Weights[:-1]):
        metrics = list()
        if len(layer["weights"].shape) == 2:
            metrics = [sum(layer["weights"][:, i]) for i in range(layer["weights"].shape[-1])]
        elif len(layer["weights"].shape) == 4:
            metrics = [sum(np.concatenate(np.concatenate(layer["weights"][:, :, :, i])))
                       for i in range(layer["weights"].shape[-1])]
        sigma = np.argsort(metrics)
        next_layer_weights = Weights[t+1]["weights"]
        new_next_layer = np.zeros(next_layer_weights.shape)

        extend_ratio = round(next_layer_weights.shape[-2]/len(sigma))

        if extend_ratio == 1:
            next_sigma = sigma
        else:
            next_sigma = np.concatenate(
                np.array([np.array(list(range(i*extend_ratio, (i+1)*extend_ratio))) for i in sigma]))

        if len(Weights[t+1]["weights"].shape) == 2:
            for j_neur in range(next_layer_weights.shape[-1]):
                new_next_layer[:, j_neur] = next_layer_weights[next_sigma, j_neur]

        elif len(Weights[t+1]["weights"].shape) == 4:
            for j_neur in range(next_layer_weights.shape[-1]):
                new_next_layer[:, :, :, j_neur] = next_layer_weights[:, :, next_sigma, j_neur]

        if len(Weights[t]["weights"].shape) == 2:
            Weights[t]["weights"] = layer["weights"][:, sigma]
        elif len(Weights[t]["weights"].shape) == 4:
            Weights[t]["weights"] = layer["weights"][:, :, :, sigma]

        Weights[t]["bias"] = layer["bias"][sigma]
        Weights[t+1]["weights"] = new_next_layer
        F_out.append(Weights[t]["weights"].flatten())
        F_out.append(Weights[t]["bias"].flatten())

    F_out.append(Weights[-1]["weights"].flatten())
    F_out.append(Weights[-1]["bias"].flatten())

    if flat_form:
        return np.concatenate(F_out)
    else:
        model_weight = list()
        for layer in Weights:
            model_weight.append(layer["weights"])
            model_weight.append(layer["bias"])
        return model_weight


class ClassifAttack():
    def __init__(self, setup_dict, protocole_dict, list_cli, list_aux, attribute, save=False, name_pb=""):
        from tensorflow.keras.utils import to_categorical
        self.save = save
        self.protocole = protocole_dict["method"]
        self.setup = setup_dict
        self.protocole.__init__(self, setup_dict)
        self.clients = list_cli
        self.auxiliaires = list_aux
        self.attribute = attribute
        self.nb_class = len(np.unique([self.data[id_cli]["metadata"]
                            [self.attribute] for id_cli in list(self.data.keys())]))
        self.attack_model = self.attack_model()
        # self.atk_weight = deepcopy(self.attack_model.get_weights())
        self.perf_client = {"van": {client: list() for client in self.clients}, "mix": {
            client: list() for client in self.clients}}
        self.Y_train_template = to_categorical(np.array(
            [self.data[id_cli]["metadata"][self.attribute] for id_cli in self.auxiliaires]), num_classes=self.nb_class)
        self.Y_test = to_categorical(np.array(
            [self.data[id_cli]["metadata"][self.attribute] for id_cli in self.clients]), num_classes=self.nb_class)
        self.X_train = np.array(list())
        self.Y_train = np.array(list())
        self.name_pb = name_pb

    def attack_model(self):

        return(RandomForestClassifier(n_estimators=200))

    def old_attack_model(self):
        """
        NN tensorflow function

        Parameters
        ----------
        X : Numpy Array
            Input Data for the NN, used to shape the first layer.

        Returns
        -------
        model : NN tensorflow
            A compiled NN from tensorflow.

        """

        from tensorflow.keras.models import Sequential
        from tensorflow.keras.layers import Dense
        from tensorflow.keras.layers import Flatten

        model = Sequential()

        model.add(Dense(50, activation='relu', kernel_initializer='he_uniform',
                  input_shape=((len(weight_to_array(self.model.get_weights())),))))
        model.add(Dense(10, activation='relu', kernel_initializer='he_uniform'))
        model.add(Dense(self.nb_class, activation='softmax'))  # , input_shape=(len(X))))

        model.compile(optimizer="Adam", loss='categorical_crossentropy', metrics=['accuracy'])

        return model

    def Build_Attack(self, Updates):
        Updates_aux = {id_client: self.protocole.update_client(
            self, id_client, self.agg_weights) for id_client in self.auxiliaires}
        Update_Mix = FederatedMixnet.MixUpdates(self, Updates)
        X_train = np.array([Canonical(Updates_aux[id_cli]) for id_cli in self.auxiliaires])
        new_X_train = np.array(list(self.X_train) + list(X_train))
        new_Y_train = np.array(list(self.Y_train) + list(self.Y_train_template))
        X_test = np.array([Canonical(Updates[id_cli]) for id_cli in self.clients])
        X_test_mix = np.array([Canonical(Update_Mix[id_cli]) for id_cli in self.clients])

        return new_X_train, new_Y_train, X_test, X_test_mix

    def compute(self, init_weight=None, list_cli=None, starting_round=0, inter_save=1):
        self.acc_attack = list()
        self.acc_attack = {"van": list(), "mix": list()}

        if init_weight == None:
            self.agg_weights = self.ini_weights
        else:
            self.agg_weights = init_weight
        if list_cli == None:
            self.list_client = list(self.data.keys())
        else:
            self.list_client = list_cli

        # Learning Rounds
        K = min(self.hparams['K'], len(self.list_client))
        for i_round, num_round in enumerate(range(1+starting_round, self.hparams['N']+1+starting_round)):
            print("\n\n######################################################################\n################## Learning round {} ################################\n######################################################################\n\n".format(
                ("000"+str(num_round))[-3:]))

            # Federated Learning : client side
            Updates = {id_client: self.protocole.update_client(
                self, id_client, self.agg_weights) for id_client in self.clients}

            # Federated Learning : Server side
            self.agg_weights = self.protocole.aggregate_updates(self, Updates)

            a1, a2, a3, a4 = self.Build_Attack(Updates)
            self.X_train = a1
            self.Y_train = a2
            X_test = a3
            X_test_mix = a4
            self.attack_model.fit(self.X_train, self.Y_train)
            if len(self.X_train) > 4000:
                self.X_train = self.X_train[:-4000]
                self.Y_train = self.Y_train[:-4000]
            inf_Ytest = self.attack_model.predict(X_test)
            acc_van = metrics.accuracy_score(self.Y_test, inf_Ytest)
            print("\nProperty inference on updates : {}.\n".format(acc_van))
            self.acc_attack["van"].append(acc_van)
            #_, acc_mix = self.attack_model.evaluate(X_test_mix, self.Y_test)
            inf_Ymix = self.attack_model.predict(X_test_mix)
            acc_mix = metrics.accuracy_score(self.Y_test, inf_Ymix)
            print("\nProperty inference on mixed updates : {}.\n".format(acc_mix))
            self.acc_attack["mix"].append(acc_mix)

            perf_van = inf_Ytest == self.Y_test
            perf_mix = inf_Ymix == self.Y_test
            for i, client in enumerate(self.clients):
                self.perf_client["van"][client].append(perf_van[i])
                self.perf_client["mix"][client].append(perf_mix[i])
            if self.save and (i_round % inter_save) == 0:
                model_name = "{}_LRClassif_R{}_E{}_B{}_N{}_K{}.sav".format(
                    self.name_pb, num_round, self.setup["hparams"]["E"], self.setup["hparams"]["B"], self.setup["hparams"]["N"], self.setup["hparams"]["K"])
                dump(self.acc_attack, open(Path("AccClassif", "0Mean_"+model_name), 'wb'))
                dump(self.perf_client, open(Path("AccClassif", "1CDF_"+model_name), 'wb'))
                # dump(self.X_train, open(Path("AccAugmClassif","X_train_K{}.json".format(self.setup["hparams"]["K"])), 'wb'))
                # dump(self.Y_train, open(Path("AccAugmClassif","Y_train_K{}.json".format(self.setup["hparams"]["K"])), 'wb'))
                # dump(X_test, open(Path("AccAugmClassif","X_test_K{}_R{}.json".format(self.setup["hparams"]["K"], i_round)), 'wb'))
                # dump(self.Y_test, open(Path("AccAugmClassif","Y_test_K{}_R{}.json".format(self.setup["hparams"]["K"], i_round)), 'wb'))

    def plot_atk(self, name_image):
        dict_accuracy = self.acc_attack

        list_rounds = list(range(len(dict_accuracy["van"])))

        plt.plot(list_rounds, dict_accuracy["van"], color="red", label="Classic")
        plt.plot(list_rounds, dict_accuracy["mix"], color="blue", label="MixNN", ls=":")

        plt.xlabel("Learning rounds", fontsize=15)
        plt.ylabel("Accuracy", fontsize=15)
        plt.grid(True)
        plt.savefig(name_image)
        plt.show()


class ClassifAugmAttack(ClassifAttack):
    def __init__(self, setup_dict, protocole_dict, list_cli, list_aux, attribute, n_augm=5, save=False, name_pb=""):
        ClassifAttack.__init__(self, setup_dict, protocole_dict, list_cli,
                               list_aux, attribute, save, name_pb)
        self.auxiliaires = list()
        self.i_round = 0

        for client_test in list_aux:

            quant_train = np.array([round((len(self.data[client_test]["x_train"])*i)/n_augm)
                                   for i in range(n_augm)]+[len(self.data[client_test]["x_train"])])

            quant_test = np.array([round((len(self.data[client_test]["x_test"])*i)/n_augm)
                                  for i in range(n_augm)]+[len(self.data[client_test]["x_test"])])

            for b_id in range(n_augm):
                new_client = client_test+str(b_id)
                self.auxiliaires.append(new_client)
                self.data[new_client] = {"x_train": self.data[client_test]["x_train"][quant_train[b_id]:quant_train[b_id+1]], "y_train": self.data[client_test]["y_train"][quant_train[b_id]:quant_train[b_id+1]],
                                         "x_test": self.data[client_test]["x_test"][quant_test[b_id]:quant_test[b_id+1]], "y_test": self.data[client_test]["y_test"][quant_test[b_id]:quant_test[b_id+1]], "metadata": self.data[client_test]["metadata"]}

    def Build_Attack(self, Updates):
        X_train = list()
        Y_train = list()
        self.i_round += 1
        for client in list(Updates.keys()):
            numo_clio = np.argwhere(np.array(self.clients) == client)
            X_train += list(np.array([Canonical2(self.protocole.update_client(self, id_client, [layer+Updates[client][i]
                            for i, layer in enumerate(self.agg_weights)]), self.i_round, numo_clio) for id_client in self.auxiliaires]))
            Y_train += list(to_categorical(np.array([self.data[id_cli]["metadata"][self.attribute]
                            for id_cli in self.auxiliaires]), num_classes=self.nb_class))
        new_X_train = np.array(list(self.X_train) + list(X_train))
        new_Y_train = np.array(list(self.Y_train) + list(Y_train))
        X_test = np.array([Canonical2(Updates[id_cli], self.i_round, numo_clio)
                          for id_cli in list(Updates.keys())])
        Update_Mix = FederatedMixnet.MixUpdates(self, Updates)
        X_test_mix = np.array([Canonical2(Update_Mix[id_cli], self.i_round, numo_clio)
                              for id_cli in list(Updates.keys())])
        return new_X_train, new_Y_train, X_test, X_test_mix


class LinkabilityAttack():

    def __init__(self, setup_dict, protocole_dict):
        self.protocole = protocole_dict["method"]
        self.protocole.__init__(self, setup_dict)
        self.acc_attack = list()
        self.perf_client = {client: list() for client in self.list_client}

    def inference_from_loss(self, Updates, client):
        nb_layers = len(self.agg_weights)

        losses = {cli: 10000000 for cli in list(Updates.keys())}

        for client_test in list(Updates.keys()):
            rebuild_weights = deepcopy(self.agg_weights)

            for n_layer in range(nb_layers):
                rebuild_weights[n_layer] += Updates[str(client_test)][n_layer]

            self.model.set_weights(rebuild_weights)

            losses[client_test] = - np.sum(np.log(self.model.predict(self.data[client]
                                           ["x_train"])[self.data[client]["y_train"] == 1]))

        self.model.set_weights(self.agg_weights)
        inf_client = min(losses, key=losses.get)
        print("Update {} infered as {}.".format(client, inf_client))
        return client == inf_client

    def protocole_attack(self, Updates_cli):
        list_inferences = [self.inference_from_loss(
            Updates_cli, client) for client in list(Updates_cli.keys())]
        temp_acc_attack = deepcopy(self.acc_attack)
        temp_acc_attack.append(np.sum(list_inferences)/len(Updates_cli))

        temp_acc_client = deepcopy(self.perf_client)

        for i, client in enumerate(list(Updates_cli.keys())):
            temp_acc_client[client].append(list_inferences[i])
        return temp_acc_attack, temp_acc_client

    def compute(self, inter_round, save=False, init_weight=None, list_cli=None, name=''):

        self.acc_attack = list()

        if init_weight == None:
            self.agg_weights = self.ini_weights
        else:
            self.agg_weights = init_weight
        if list_cli == None:
            self.list_client = list(self.data.keys())
        else:
            self.list_client = list_cli

        # Learning Rounds
        K = min(self.hparams['K'], len(self.list_client))
        for i_round, num_round in enumerate(range(1, self.hparams['N']+1)):
            print("\n\n######################################################################\n################## Learning round {} ################################\n######################################################################\n\n".format(
                ("000"+str(num_round))[-3:]))

            # Federated Learning : client side
            selected_clients = sample(self.list_client, K)

            if (num_round/inter_round - num_round//inter_round) == 0.0 or i_round == 0 or num_round == self.hparams['N']:
                # Linkability Attack from Server
                self.acc_attack, self.perf_client = self.protocole_attack(Updates_cli)
                if save:
                    dump(self.acc_attack, open(
                        Path("AccLink", "0Mean_TempSaveR{}_{}.sav".format(i_round, name)), 'wb'))
                    dump(self.perf_client, open(
                        Path("AccLink", "1CDF_TempSaveR{}_{}.sav".format(i_round, name)), 'wb'))
            # Federated Learning : Server side
            self.agg_weights = self.protocole.aggregate_updates(self, Updates_cli)

            # Federated Learning : Aggregated model evaluation
            # self.protocole.evaluate_model(self,i_round)#Useless in our case

    def plot_atk(self, name_image):
        plt.plot(list(range(len(self.acc_attack))), self.acc_attack, color="red")
        plt.xlabel("Learning rounds", fontsize=15)
        plt.ylabel("Accuracy", fontsize=15)
        plt.grid(True)
        plt.savefig(name_image)
        plt.show()


class LinkabilityAttackMixnet(LinkabilityAttack):

    def inference_from_loss(self, Updates, client, key):
        # one of two, to avoid the bias weights which are always equal to zeros : they are useless
        nb_layers = round(len(self.agg_weights)//2)

        losses = {n_layer: {cli: 10000000 for cli in list(
            Updates.keys())} for n_layer in range(nb_layers)}
        lower_cli_layer = list()
        lower_loss_layer = list()
        for n_layer in range(nb_layers):
            rebuild_weights = deepcopy(self.agg_weights)
            rebuild_weights[n_layer*2] += Updates[str(client)][n_layer*2]
            self.model.set_weights(rebuild_weights)
            loss_list = list()

            for client_test in list(Updates.keys()):
                loss_val = - \
                    np.sum(np.log(self.model.predict(self.data[client_test]["x_train"])[
                           self.data[client_test]["y_train"] == 1]))
                # WARNING IF NOT CATEGORICAL CROSS ENTROPY
                losses[n_layer][client_test] = loss_val
                loss_list.append(loss_val)

            lower_cli_layer.append(list(Updates.keys())[np.argmin(np.array(loss_list))])
            lower_loss_layer.append(min(losses[n_layer].values()))

        weakest_layer = np.argmin(np.array(lower_loss_layer))
        infered_client_weakest_layer = lower_cli_layer[weakest_layer]
        corresponding_client = list(Updates.keys())[key[weakest_layer*2]]
        self.model.set_weights(self.agg_weights)
        print("Layer {} : {} infered as {}.".format(weakest_layer,
              infered_client_weakest_layer, corresponding_client))
        return infered_client_weakest_layer == corresponding_client

    def protocole_attack(self, Updates_cli):
        Updates_cli, key_mixnet = FederatedMixnet.MixUpdates(self, Updates_cli, get_key=True)

        list_inferences = [self.inference_from_loss(
            Updates_cli, client, key_mixnet[:, id_client]) for id_client, client in enumerate(list(Updates_cli.keys()))]
        temp_acc_attack = deepcopy(self.acc_attack)
        temp_acc_attack.append(np.sum(list_inferences)/len(Updates_cli))

        temp_acc_client = deepcopy(self.perf_client)

        for i, client in enumerate(list(Updates_cli.keys())):
            temp_acc_client[client].append(list_inferences[i])
        return temp_acc_attack, temp_acc_client


class UnMixnet(LinkabilityAttack):

    def inference_from_loss(self, Updates, client, key):
        # one of two, to avoid the bias weights which are always equal to zeros : they are useless
        nb_layers = round(len(self.agg_weights)//2)
        list_cli = list()
        X = self.data[client]["x_train"]
        Y = self.data[client]["y_train"]
        rebuild_weights = deepcopy(self.agg_weights)
        for n_layer in range(nb_layers):
            loss_list = list()
            current_rebuild = deepcopy(rebuild_weights)
            for client_test in list(Updates.keys()):
                rebuild_weights[n_layer*2] += Updates[str(client_test)][n_layer*2]
                self.model.set_weights(rebuild_weights)

                loss_val = - np.sum(np.log(self.model.predict(X)[Y == 1]))
                # WARNING IF NOT CATEGORICAL CROSS ENTROPY
                loss_list.append(loss_val)
                rebuild_weights = deepcopy(current_rebuild)

            best_cli = list(Updates.keys())[key[n_layer, np.argmin(loss_list)]]
            list_cli.append(best_cli)
            rebuild_weights[n_layer*2] += Updates[best_cli][n_layer*2]
        score = np.mean(np.array(list_cli) == client)
        self.model.set_weights(self.agg_weights)
        print("Client {} model infered at {}%.".format(client, score))
        return score

    def protocole_attack(self, Updates_cli):
        Updates_cli, key_mixnet = FederatedMixnet.MixUpdates(self, Updates_cli, get_key=True)

        list_inferences = [self.inference_from_loss(
            Updates_cli, client, key_mixnet) for client in list(Updates_cli.keys())]
        temp_acc_attack = deepcopy(self.acc_attack)
        temp_acc_attack.append(np.sum(list_inferences)/len(Updates_cli))

        temp_acc_client = deepcopy(self.perf_client)

        for i, client in enumerate(list(Updates_cli.keys())):
            temp_acc_client[client].append(list_inferences[i])
        return temp_acc_attack, temp_acc_client


class DSimAttack():

    def __init__(self, setup_dict, protocole_dict):
        self.setup_dict = setup_dict
        self.protocole_dict = protocole_dict
        self.protocole = protocole_dict["method"]
        self.protocole.__init__(self, setup_dict)
        self.acc_attack = {"van": list(), "mix": list()}
        self.property = setup_dict["attribute"]

    def agg_retrain(self, dic_weights, dic_len_data, init_weights):
        atk_model = deepcopy(init_weights)
        nb_layers = len(init_weights)
        len_tot = sum(dic_len_data.values())
        for n_layer in range(nb_layers):
            layer = np.zeros(init_weights[n_layer].shape)

            for id_distrib, w_model in enumerate(dic_weights.values()):
                layer += (dic_len_data[str(id_distrib)]/len_tot)*w_model[n_layer]

            atk_model[n_layer] = deepcopy(layer)

        return atk_model

    def model_retrain(self, list_distribs, ini_weight, dict_split, i, b_id, n_retrain=1):
        w_out = dict()

        for id_distrib, modality in enumerate(list_distribs.keys()):
            print("\n\n######################################################################\n#### Group {} Retrain Learning round {} ##############################\n######################################################################\n\n".format(
                id_distrib, ("000"+str(i))[-3:]))
            self.atk_protocole = self.protocole_dict["method"](self.setup_dict)
            self.atk_protocole.hparams['N'] = n_retrain
            self.atk_protocole.compute(
                deepcopy(ini_weight), dict_split["train"][str(id_distrib)][b_id])
            w_out[str(id_distrib)] = deepcopy(self.atk_protocole.agg_weights)

        return w_out

    def plot_atk(self, name_image):
        dict_accuracy = self.acc_attack

        list_rounds = list(range(len(dict_accuracy["van"])))

        plt.plot(list_rounds, dict_accuracy["van"], color="red", label="Classic")
        plt.plot(list_rounds, dict_accuracy["mix"], color="blue", label="MixNN", ls=":")

        plt.xlabel("Learning rounds", fontsize=15)
        plt.ylabel("Accuracy", fontsize=15)
        plt.grid(True)
        plt.savefig(name_image)
        plt.show()

    def split_client_cross_val(self):
        dict_split = {"train": {}, "test": {"b0": list(), "b1": list(), "b2": list()},
                      "distrib": {}}

        list_cli = np.array(list(self.data.keys()))

        modalities = [self.data[client]["metadata"][self.property] for client in list_cli]

        list_distribs = {mod: list(list_cli[list(np.where(modalities == mod)[0])])
                         for mod in np.unique(modalities)}

        for id_distrib, modality in enumerate(list_distribs.keys()):
            dict_split["train"][str(id_distrib)] = dict()
            list_cli = list_distribs[modality]
            n1, n2 = int((len(list_cli)*0.33)//1), int((len(list_cli)*0.66)//1)
            dict_split["test"]["b0"] += list_cli[:n1]
            dict_split["test"]["b1"] += list_cli[n1:n2]
            dict_split["test"]["b2"] += list_cli[n2:]
            dict_split["train"][str(id_distrib)]["b0"], dict_split["train"][str(id_distrib)]["b1"], dict_split["train"][str(
                id_distrib)]["b2"] = list_cli[n1:], list_cli[:n1] + list_cli[n2:], list_cli[:n2]

            ### spec_weights[str(id_distrib)] = deepcopy(init_weights)
            for client in list_cli:
                dict_split["distrib"][client] = id_distrib

        return dict_split, list_distribs

    def list_client_fedtrain(self, item):
        return item

    def compute(self, n_retrain, inter_test_attack=1, batch_id="b0"):
        '''
        function description.

                Parameters:
                        input (type): Description.

                Returns:
                        output (type): Description.
        '''

        self.dict_acc = {str(N): {"van": list(), "mix": list()}
                         for N in range(1, self.hparams["N"]+1)}

        d_split, list_distribs = self.split_client_cross_val()

        FedTraining = self.protocole_dict["method"](self.setup_dict)
        FedMixnet = FederatedMixnet(self.setup_dict, self.protocole_dict)
        init_weights = deepcopy(FedTraining.agg_weights)
        agg_weights = deepcopy(FedTraining.agg_weights)
        dic_weights = {str(id_distrib): deepcopy(init_weights)
                       for id_distrib, modality in enumerate(list_distribs.keys())}

        dic_len_data = {str(id_distrib): sum([len(self.data[cli]) for cli in d_split["train"][str(
            id_distrib)][batch_id]]) for id_distrib, modality in enumerate(list_distribs.keys())}
        self.perf_client = {"van": {client: list() for client in d_split["test"][batch_id]}, "mix": {
            client: list() for client in d_split["test"][batch_id]}}
        for i in range(1, self.hparams["N"]+1, inter_test_attack):

            # init of the methods

            FedTraining.hparams['N'] = inter_test_attack
            FedTraining.compute(init_weight=agg_weights,
                                list_cli=self.list_client_fedtrain(d_split["test"][batch_id]))
            ini_weight = deepcopy(FedTraining.agg_weights)

            dic_weights = self.model_retrain(
                list_distribs, ini_weight, d_split, i, batch_id, n_retrain)

            # attack model computation
            print("\n\n######################################################################\n#### Attack model computation : Learning round {} ###################\n######################################################################\n\n".format(
                ("000"+str(i))[-3:]))
            atk_weight = self.agg_retrain(dic_weights, dic_len_data, init_weights)

            # balanced update

            len_tot = sum(dic_len_data.values())

            list_updates = [((dic_len_data[str(id_distrib)]/len_tot) * weight_to_array(w_model)-weight_to_array(deepcopy(atk_weight)))
                            for id_distrib, w_model in enumerate(dic_weights.values())]  # rajouter le facteur taille relative

            print("\n\n######################################################################\n#### Clients model computation for attack : Learning round {} #######\n######################################################################\n\n".format(
                ("000"+str(i))[-3:]))
            Updates_Van = {client_test: FedMixnet.protocole.update_client(
                self, client_test, deepcopy(atk_weight)) for client_test in d_split["test"][batch_id]}
            Updates_Mix = FedMixnet.MixUpdates(Updates_Van)

            nb_tot, nb_good, nb_mixed = 0, 0, 0
            for client_test in d_split["test"][batch_id]:
                distrib_val = d_split["distrib"][client_test]

                list_sim = [np.vdot(weight_to_array(Updates_Van[str(client_test)]), model_update)/(np.linalg.norm(
                    weight_to_array(Updates_Van[str(client_test)]))*np.linalg.norm(model_update)) for model_update in list_updates]
                inf_true = list_sim.index(max(list_sim))

                list_sim_mix = [np.vdot(weight_to_array(Updates_Mix[str(client_test)]), model_update)/(np.linalg.norm(
                    weight_to_array(Updates_Mix[str(client_test)]))*np.linalg.norm(model_update)) for model_update in list_updates]
                inf_mixed = list_sim_mix.index(max(list_sim_mix))

                print("Client {} property inference {} : Inference : Van {}/ Mix {} \n".format(client_test,
                      distrib_val, inf_true, inf_mixed))

                nb_good += (distrib_val == inf_true)

                nb_mixed += (distrib_val == inf_mixed)
                self.perf_client["van"][client_test].append(distrib_val == inf_true)
                self.perf_client["mix"][client_test].append(distrib_val == inf_mixed)
                nb_tot += 1

            self.dict_acc[str(i)]["van"].append(nb_good/nb_tot)
            self.dict_acc[str(i)]["mix"].append(nb_mixed/nb_tot)


class ContaDSimAttack(DSimAttack):
    def list_client_fedtrain(self, item):
        # return None for the list of clients in the federated learning process : all the clients (the auxiliaries and the normals) are used to build the aggregated model
        return None


class EqDSimAttack(DSimAttack):
    def compute(self, n_retrain, inter_test_attack=1, batch_id="b0"):
        '''
        function description.

                Parameters:
                        input (type): Description.

                Returns:
                        output (type): Description.
        '''

        self.dict_acc = {str(N): {"van": list(), "mix": list()}
                         for N in range(1, self.hparams["N"]+1)}

        d_split, list_distribs = self.split_client_cross_val()

        FedTraining = self.protocole_dict["method"](self.setup_dict)
        FedMixnet = FederatedMixnet(self.setup_dict, self.protocole_dict)
        init_weights = deepcopy(FedTraining.agg_weights)
        agg_weights = deepcopy(FedTraining.agg_weights)
        dic_weights = {str(id_distrib): deepcopy(init_weights)
                       for id_distrib, modality in enumerate(list_distribs.keys())}

        dic_len_data = {str(id_distrib): sum([len(self.data[cli]) for cli in d_split["train"][str(
            id_distrib)][batch_id]]) for id_distrib, modality in enumerate(list_distribs.keys())}
        self.perf_client = {"van": {client: list() for client in d_split["test"][batch_id]}, "mix": {
            client: list() for client in d_split["test"][batch_id]}}
        for i in range(1, self.hparams["N"]+1, inter_test_attack):

            # init of the methods

            FedTraining.hparams['N'] = inter_test_attack
            FedTraining.compute(init_weight=agg_weights, list_cli=d_split["test"][batch_id])
            ini_weight = deepcopy(FedTraining.agg_weights)

            dic_weights = self.model_retrain(
                list_distribs, ini_weight, d_split, i, batch_id, n_retrain)

            # attack model computation
            print("\n\n######################################################################\n#### Attack model computation : Learning round {} ###################\n######################################################################\n\n".format(
                ("000"+str(i))[-3:]))
            atk_weight = self.agg_retrain(dic_weights, dic_len_data, init_weights)

            list_updates = [(weight_to_array(w_model)-weight_to_array(deepcopy(atk_weight)))
                            for w_model in dic_weights.values()]  # rajouter le facteur taille relative

            print("\n\n######################################################################\n#### Clients model computation for attack : Learning round {} #######\n######################################################################\n\n".format(
                ("000"+str(i))[-3:]))
            Updates_Van = {client_test: FedMixnet.protocole.update_client(
                self, client_test, deepcopy(atk_weight)) for client_test in d_split["test"][batch_id]}
            Updates_Mix = FedMixnet.MixUpdates(Updates_Van)

            nb_tot, nb_good, nb_mixed = 0, 0, 0
            for client_test in d_split["test"][batch_id]:
                distrib_val = d_split["distrib"][client_test]

                list_sim = [np.vdot(weight_to_array(Updates_Van[str(client_test)]), model_update)/(np.linalg.norm(
                    weight_to_array(Updates_Van[str(client_test)]))*np.linalg.norm(model_update)) for model_update in list_updates]
                inf_true = list_sim.index(max(list_sim))

                list_sim_mix = [np.vdot(weight_to_array(Updates_Mix[str(client_test)]), model_update)/(np.linalg.norm(
                    weight_to_array(Updates_Mix[str(client_test)]))*np.linalg.norm(model_update)) for model_update in list_updates]
                inf_mixed = list_sim_mix.index(max(list_sim_mix))

                print("Client {} property inference {} : Inference : Van {}/ Mix {} \n".format(client_test,
                      distrib_val, inf_true, inf_mixed))

                nb_good += (distrib_val == inf_true)

                nb_mixed += (distrib_val == inf_mixed)
                self.perf_client["van"][client_test].append(distrib_val == inf_true)
                self.perf_client["mix"][client_test].append(distrib_val == inf_mixed)
                nb_tot += 1

            self.dict_acc[str(i)]["van"].append(nb_good/nb_tot)
            self.dict_acc[str(i)]["mix"].append(nb_mixed/nb_tot)


class ContaEqDSimAttack(DSimAttack):
    def list_client_fedtrain(self, item):
        # return None for the list of clients in the federated learning process : all the clients (the auxiliaries and the normals) are used to build the aggregated model
        return None

    def compute(self, n_retrain, inter_test_attack=1, batch_id="b0"):
        '''
        function description.

                Parameters:
                        input (type): Description.

                Returns:
                        output (type): Description.
        '''

        self.dict_acc = {str(N): {"van": list(), "mix": list()}
                         for N in range(1, self.hparams["N"]+1)}

        d_split, list_distribs = self.split_client_cross_val()

        FedTraining = self.protocole_dict["method"](self.setup_dict)
        FedMixnet = FederatedMixnet(self.setup_dict, self.protocole_dict)
        init_weights = deepcopy(FedTraining.agg_weights)
        agg_weights = deepcopy(FedTraining.agg_weights)
        dic_weights = {str(id_distrib): deepcopy(init_weights)
                       for id_distrib, modality in enumerate(list_distribs.keys())}

        dic_len_data = {str(id_distrib): sum([len(self.data[cli]) for cli in d_split["train"][str(
            id_distrib)][batch_id]]) for id_distrib, modality in enumerate(list_distribs.keys())}
        self.perf_client = {"van": {client: list() for client in d_split["test"][batch_id]}, "mix": {
            client: list() for client in d_split["test"][batch_id]}}
        for i in range(1, self.hparams["N"]+1, inter_test_attack):

            # init of the methods

            FedTraining.hparams['N'] = inter_test_attack
            FedTraining.compute(init_weight=agg_weights)  # , list_cli = d_split["test"][batch_id])
            ini_weight = deepcopy(FedTraining.agg_weights)

            dic_weights = self.model_retrain(
                list_distribs, ini_weight, d_split, i, batch_id, n_retrain)

            # attack model computation
            print("\n\n######################################################################\n#### Attack model computation : Learning round {} ###################\n######################################################################\n\n".format(
                ("000"+str(i))[-3:]))
            atk_weight = self.agg_retrain(dic_weights, dic_len_data, init_weights)

            list_updates = [(weight_to_array(w_model)-weight_to_array(deepcopy(atk_weight)))
                            for w_model in dic_weights.values()]  # rajouter le facteur taille relative

            print("\n\n######################################################################\n#### Clients model computation for attack : Learning round {} #######\n######################################################################\n\n".format(
                ("000"+str(i))[-3:]))
            Updates_Van = {client_test: FedMixnet.protocole.update_client(
                self, client_test, deepcopy(atk_weight)) for client_test in d_split["test"][batch_id]}
            Updates_Mix = FedMixnet.MixUpdates(Updates_Van)

            nb_tot, nb_good, nb_mixed = 0, 0, 0
            for client_test in d_split["test"][batch_id]:
                distrib_val = d_split["distrib"][client_test]

                list_sim = [np.vdot(weight_to_array(Updates_Van[str(client_test)]), model_update)/(np.linalg.norm(
                    weight_to_array(Updates_Van[str(client_test)]))*np.linalg.norm(model_update)) for model_update in list_updates]
                inf_true = list_sim.index(max(list_sim))

                list_sim_mix = [np.vdot(weight_to_array(Updates_Mix[str(client_test)]), model_update)/(np.linalg.norm(
                    weight_to_array(Updates_Mix[str(client_test)]))*np.linalg.norm(model_update)) for model_update in list_updates]
                inf_mixed = list_sim_mix.index(max(list_sim_mix))

                print("Client {} property inference {} : Inference : Van {}/ Mix {} \n".format(client_test,
                      distrib_val, inf_true, inf_mixed))

                nb_good += (distrib_val == inf_true)

                nb_mixed += (distrib_val == inf_mixed)
                self.perf_client["van"][client_test].append(distrib_val == inf_true)
                self.perf_client["mix"][client_test].append(distrib_val == inf_mixed)
                nb_tot += 1

            self.dict_acc[str(i)]["van"].append(nb_good/nb_tot)
            self.dict_acc[str(i)]["mix"].append(nb_mixed/nb_tot)
