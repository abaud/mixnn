from Method import ClassifAttack
from setup import setup, protocole
import pickle
from numpy.random import shuffle
from numpy import array
from pathlib import Path

# Method = "FedAvg"#"FedAvg","FedNoises","FedMed","FedPruning16bit"
# Problem = "MotionSense"#"Cifar10","MotionSense","MobiAct","LFW"
# batch_CV = "b0"#"b0","b1","b2"

import sys
try :
    Method, Problem, batch_CV = sys.argv[1], sys.argv[2], sys.argv[3]
except:
    Method = "FedAvg"#"FedAvg","FedNoises","FedMed","FedPruning16bit"
    Problem = "LFW"#"Cifar10","MotionSense","MobiAct","LFW"
    batch_CV = "b0"#"b0","b1","b2"

config = setup(Problem)
config_method = protocole(Method)
clients = array(list(config["dataset"].keys()))
config['hparams']["N"] = 40
#CrossVal
def split_client_cross_val(dataset):
    dict_split = {"train" : {"b0" : list(), "b1" : list(), "b2" : list()}, "test" : {"b0" : list(), "b1" : list(), "b2" : list()}}
    
    list_cli = list(dataset.keys())
    n1, n2 = int((len(list_cli)*0.33)//1), int((len(list_cli)*0.66)//1)
    dict_split["test"]["b0"] += list_cli[:n1]
    dict_split["test"]["b1"] += list_cli[n1:n2]
    dict_split["test"]["b2"] += list_cli[n2:]

    dict_split["train"]["b0"], dict_split["train"]["b1"], dict_split["train"]["b2"] = list_cli[n1:], list_cli[:n1] + list_cli[n2:], list_cli[:n2]
    
            
    return dict_split

split_dict = split_client_cross_val(config["dataset"])
list_cli, list_aux = split_dict["test"][batch_CV], split_dict["train"][batch_CV]

Federated = ClassifAttack(config, config_method, list_cli, list_aux, config["attribute"])
Federated.compute()

model_name = "{}_ReducedClassif_E{}_B{}_N{}_K{}.sav".format(Problem+Method+batch_CV,config["hparams"]["E"], config["hparams"]["B"], config["hparams"]["N"], config["hparams"]["K"])
# Federated.plot_atk("Images\\{}.png".format("Classif"+Problem+Method))
pickle.dump(Federated.acc_attack, open(Path("AccClassif","0Mean_"+model_name), 'wb'))
pickle.dump(Federated.perf_client, open(Path("AccClassif","1CDF_"+model_name), 'wb'))
