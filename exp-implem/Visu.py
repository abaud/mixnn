from pathlib import Path
import pickle
import matplotlib.pyplot as plt
import numpy as np
import os
from setup import setup

Dict_Legendes = {"FedAvg" : {"color" : "red", "label" : "FedAVG", "linewidth" : 1, "ls" : "-"},
                 "MFedAvg" : {"color" : "k", "label" : "Mixnet FedAVG", "linewidth" : 5, "ls" : ":"},
                 "16bit" : {"color" : "blue", "label" : "Pruning 16bit", "linewidth" : 1, "ls" : "-."},
                 "32bit" : {"color" : "green", "label" : "Pruning 32bit", "linewidth" : 1, "ls" : "-"},
                 "8bit" : {"color" : "green", "label" : "Pruning 8bit", "linewidth" : 3, "ls" : "--"},
                 "Noised" : {"color" : "grey", "label" : "Noised", "linewidth" : 1, "ls" : "-"},
                 "Rand" : {"color" : "k", "label" : "Random Guess", "linewidth" : 1, "ls" : "--"},
                 "Cifar10" : {"color" : "red", "label" : "Cifar10", "linewidth" : 1, "ls" : "-"},
                 "MotionSense" : {"color" : "k", "label" : "MotionSense", "linewidth" : 3, "ls" : "--"},
                 "MobiAct" : {"color" : "green", "label" : "MobiAct", "linewidth" : 1, "ls" : "-."},
                 "LFW" : {"color" : "blue", "label" : "LFW", "linewidth" : 1, "ls" : "--"}
                }

def evaluate_random(dict_setup):
    list_modalities = np.array([dict_setup["dataset"][client]["metadata"][dict_setup["attribute"]] for client in dict_setup["dataset"].keys()])
    out = [np.sum(list_modalities == mod) for mod in np.unique(list_modalities)]
    return max(out)/len(dict_setup["dataset"].keys()), np.unique(list_modalities)[np.argmax(out)]

def ExpUtility_loader(name, path, key_method = ''):
    """
    Merge batches of Utility experiments.
    """
    out = dict()
    dir_list = os.listdir(path)
    for dirname in dir_list:
        if (name in dirname) and (key_method in dirname):
            #print(dirname)
            Accuracy = pickle.load(open(Path(path,dirname), "rb"))

            if len(out)==0:
                for key in list(Accuracy.keys()):
                    out[key] = [list() for i in range(len(Accuracy[key]))]
                    
            for key in list(Accuracy.keys()):
                for i in range(len(out[key])):
                    out[key][i].append(Accuracy[key][i])
    return out

def Link_loader(name, path, key_method = '', pprint = False):
    """
    Merge batches of Utility experiments.
    """
    out = list()
    dir_list = os.listdir(path)
    if key_method == '':
        cond = "Link"
    else:
        cond = key_method
    for dirname in dir_list:
        if (name in dirname) and (cond in dirname):
            #print(dirname)
            Accuracy = pickle.load(open(Path(path,dirname), "rb"))
            # if pprint:
            #     print(Accuracy)
            if len(out)==0:
                out = [list() for i in range(len(Accuracy))]
                    
            
            for i in range(len(out)):
                out[i].append(Accuracy[i])
    return out

def UtilityBuilder(Dataset, PathFile = "AccMixnet", key_method = ''):
    out = {"FedAvg" : ExpUtility_loader("0Van_{}FedAvg".format(Dataset), PathFile, key_method),
                   # "MFedAvg" : ExpUtility_loader("1Mix_{}FedAvg".format(Dataset), PathFile, key_method),
                   # "16bit" : ExpUtility_loader("0Van_{}FedPruning16bit".format(Dataset), PathFile, key_method),
                   # "8bit" : ExpUtility_loader("0Van_{}FedPruning8bit".format(Dataset), PathFile, key_method),
                   # "Noised" : ExpUtility_loader("0Van_{}FedNoises1".format(Dataset), PathFile, key_method)
                   }
    return out

def VanUBuilder(Dataset, PathFile = "TempAcc", key_method = ''):
    out = {"FedAvg" : ExpUtility_loader("0Van_{}FedAvg".format(Dataset), PathFile, key_method),
                   "MFedAvg" : ExpUtility_loader("1Mix_{}FedAvg".format(Dataset), PathFile, key_method),
                   "16bit" : ExpUtility_loader("0Van_{}FedPruning16bit".format(Dataset), PathFile, key_method)}
    return out


def ClassifExpCDF_loader(name, path, pb):
    """
    Merge batches of Utility experiments.
    """
    list_cli = list(setup(pb)["dataset"].keys())
    out = {"van" : {client : list() for client in list_cli}, "mix" : {client : list() for client in list_cli}}
    dir_list = os.listdir(path)
    for dirname in dir_list:
        if name in dirname:
            #print(dirname)
            Accuracy = pickle.load(open(Path(path,dirname), "rb"))  
            for key in list(Accuracy.keys()):
                for client in list(Accuracy[key].keys()):
                    out[key][client] += list(np.sum(Accuracy[key][client], axis = 1)>=np.array(Accuracy[key][client]).shape[1])
    for key in list(out.keys()):
        for client in list(out[key].keys()):
            out[key][client] = np.mean(out[key][client])
    return out

def ExpCDF_loader(name, path, pb):
    """
    Merge batches of Utility experiments.
    """
    list_cli = list(setup(pb)["dataset"].keys())
    out = {"van" : {client : list() for client in list_cli}, "mix" : {client : list() for client in list_cli}}
    dir_list = os.listdir(path)
    for dirname in dir_list:
        if name in dirname:
            #print(dirname)
            Accuracy = pickle.load(open(Path(path,dirname), "rb"))  
            for key in list(Accuracy.keys()):
                for client in list(Accuracy[key].keys()):
                    out[key][client] += list(Accuracy[key][client])
    for key in list(out.keys()):
        for client in list(out[key].keys()):
            out[key][client] = np.mean(out[key][client])
    return out



def PrivacyBuilder(Dataset, PathFile, key_method = ""):
    temp =  ExpUtility_loader("0Mean_{}FedAvg".format(Dataset), PathFile, key_method)
    out = {"FedAvg" : temp['van'],
                "MFedAvg" : temp['mix'],
                "16bit" : ExpUtility_loader("0Mean_{}FedPruning16bit".format(Dataset), PathFile)['van'],
                "8bit" : ExpUtility_loader("0Mean_{}FedPruning8bit".format(Dataset), PathFile)['van'],
                "Noised" : ExpUtility_loader("0Mean_{}FedNoises1".format(Dataset), PathFile)['van']}
    return out


def LBuilder(Dataset, PathFile, key_method = ""):
    out = {"FedAvg" : Link_loader("0Mean_{}FedAvg".format(Dataset), PathFile, key_method),
                "MFedAvg" : Link_loader("0Mean_{}MixFedAvg".format(Dataset), PathFile, key_method, pprint = True),
                "16bit" : Link_loader("0Mean_{}FedPruning16bit".format(Dataset), PathFile, key_method),
                "8bit" : Link_loader("0Mean_{}FedPruning8bit".format(Dataset), PathFile, key_method),
                "Noised" : Link_loader("0Mean_{}FedNoises1".format(Dataset), PathFile, key_method)}
    return out

def UnMixBuilder():
    PathFile = "AccLink"
    key_method = "UnMixnet"
    out = {Dataset : Link_loader("0Mean_{}FedAvg".format(Dataset), PathFile, key_method) for Dataset in ["Cifar10","MotionSense", "MobiAct", "LFW"]}
    return out

def PrivVanBuilder(Dataset, PathFile, key_method = ""):
    temp =  ExpUtility_loader("0Mean_{}FedAvg".format(Dataset), PathFile, key_method)
    out = {"FedAvg" : temp['van'],
                "MFedAvg" : temp['mix']}
    return out


def ExpDSim_loader(name, path, key_method = ""):
    """
    Merge batches of Utility experiments.
    """
    out = {"van" : list(), "mix" :list()}
    dir_list = os.listdir(path)
    for dirname in dir_list:
        if (name in dirname) and (key_method in dirname):
            print(dirname)
            Accuracy = pickle.load(open(Path(path,dirname), "rb"))
            for key in list(Accuracy.keys()):
                for client in list(Accuracy[key].keys()):
                    out[key].append(Accuracy[key][client])
    out["van"], out["mix"] = np.mean(np.array(out["van"]), axis = 0), np.mean(np.array(out["mix"]), axis = 0)
    return out

def DSimBuilder(Dataset, PathFile, key_method = ""):
    temp =  ExpDSim_loader("1CDF_{}FedAvg".format(Dataset), PathFile, key_method = key_method)
    out = {"FedAvg" : temp['van'],
                "MFedAvg" : temp['mix'],
                "16bit" : ExpDSim_loader("1CDF_{}FedPruning16bit".format(Dataset), PathFile, key_method = key_method)['van'],
                "8bit" : ExpDSim_loader("1CDF_{}FedPruning8bit".format(Dataset), PathFile, key_method = key_method)['van'],
                "Noised" : ExpDSim_loader("1CDF_{}FedNoises".format(Dataset), PathFile, key_method = key_method)['van']}
    return out



def ClassifCDFPrivacyBuilder(Dataset, PathFile):
    temp = ClassifExpCDF_loader("1CDF_{}FedAvg".format(Dataset), PathFile, Dataset)
    out = {"FedAvg" : temp['van'],
                "MFedAvg" : temp['mix'],
                "16bit" : ClassifExpCDF_loader("1CDF_{}FedPruning16bit".format(Dataset), PathFile, Dataset)['van'],
                "8bit" : ClassifExpCDF_loader("1CDF_{}FedPruning8bit".format(Dataset), PathFile, Dataset)['van'],
                "Noised" : ClassifExpCDF_loader("1CDF_{}FedNoises".format(Dataset), PathFile, Dataset)['van']}
    return out

def CDFPrivacyBuilder(Dataset, PathFile):
    temp = ExpCDF_loader("1CDF_{}FedAvg".format(Dataset), PathFile, Dataset)
    out = {"FedAvg" : temp['van'],
                "MFedAvg" : temp['mix'],
                "16bit" : ExpCDF_loader("1CDF_{}FedPruning16bit".format(Dataset), PathFile, Dataset)['van'],
                "8bit" : ExpCDF_loader("1CDF_{}FedPruning8bit".format(Dataset), PathFile, Dataset)['van'],
                "Noised" : ExpCDF_loader("1CDF_{}FedNoises".format(Dataset), PathFile, Dataset)['van']}
    return out

def ExpLCDF_loader(name, path, pb):
    """
    Merge batches of Utility experiments.
    """
    list_cli = list(setup(pb)["dataset"].keys())
    out = {client : list() for client in list_cli}
    dir_list = os.listdir(path)
    for dirname in dir_list:
        if name in dirname:
            #print(dirname)
            Accuracy = pickle.load(open(Path(path,dirname), "rb"))  
            for client in list(Accuracy.keys()):
                out[client] += list(Accuracy[client])
    for client in list(out.keys()):
        out[client] = np.mean(out[client])
    return out


def LCDFPrivacyBuilder(Dataset, PathFile):
    out = {"FedAvg" : ExpLCDF_loader("1CDF_{}FedAvg".format(Dataset), PathFile, Dataset),
                "MFedAvg" : ExpLCDF_loader("1CDF_{}MixFedAvg".format(Dataset), PathFile, Dataset),
                "16bit" : ExpLCDF_loader("1CDF_{}FedPruning16bit".format(Dataset), PathFile, Dataset),
                "8bit" : ExpLCDF_loader("1CDF_{}FedPruning8bit".format(Dataset), PathFile, Dataset),
                "Noised" : ExpLCDF_loader("1CDF_{}FedNoises".format(Dataset), PathFile, Dataset)}
    return out

def ClassifLoader(name, path):
    """
    Merge batches of Classif experiments.
    """
    out = dict()
    dir_list = os.listdir(path)
    for dirname in dir_list:
        if name in dirname:
            #print(dirname)
            Accuracy = pickle.load(open(Path(path,dirname), "rb"))
            if len(out)==0:
                for key in list(Accuracy.keys()):
                    out[key] = [list() for i in range(len(Accuracy[key]))]
                    
            for key in list(Accuracy.keys()):
                for i in range(len(out[key])):
                    out[key][i].append(Accuracy[key][i])
    return out

def plot_model_f5(pb, builder = UtilityBuilder):
    Experiences = builder(pb)
    for protocole in list(Experiences.keys()):
        list_client = list(Experiences[protocole].keys())
        list_rounds = list(range(len(Experiences[protocole][str(list_client[0])])))
        plt.plot(list_rounds, [np.mean([Experiences[protocole][str(client)][rounds] for client in list_client]) for rounds in list_rounds], color = Dict_Legendes[protocole]["color"], label = Dict_Legendes[protocole]["label"], linewidth = Dict_Legendes[protocole]["linewidth"], ls = Dict_Legendes[protocole]["ls"])
    plt.xlabel("Learning rounds", fontsize = 15)
    plt.ylabel("Utility Accuracy", fontsize = 15)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.grid(True)
    #plt.ylim(ymin=0, ymax=1)
    plt.savefig("Images\\Temp_fig5_"+pb+".png", bbox_inches="tight")
    plt.show()



def plot_model_f6(pb, num_round = -1, size = 20):
    Experiences = UtilityBuilder(pb)
    scales = (np.array(list(range(1,size+1)))/size).tolist()
    for protocole in list(Experiences.keys()):
        list_client = list(Experiences[protocole].keys())
        nb_tot = len(list_client)
        list_acc = np.array([Experiences[protocole][str(client)][num_round] for client in list_client])
        plt.plot([[np.mean(sum(list_acc<acc_model))/nb_tot for acc_model in scales][rounds] for rounds in range(len(scales))], scales, color = Dict_Legendes[protocole]["color"], label = Dict_Legendes[protocole]["label"], linewidth = Dict_Legendes[protocole]["linewidth"], ls = Dict_Legendes[protocole]["ls"])

    plt.xlabel("Clients ratio", fontsize = 15)
    plt.ylabel("Utility Accuracy", fontsize = 15)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.grid(True)
    plt.savefig("Images\\fig6_"+pb+".png", bbox_inches="tight")
    plt.show()


def plot_unmixnet():
    Experiences = UnMixBuilder()
    # step = 5
    for protocole in list(Experiences.keys()):
        list_rounds = np.array(list(range(len(Experiences[protocole]))))
        list_van = [np.mean(Experiences[protocole][rounds]) for rounds in list_rounds]
        step = 40//len(list_van)
        plt.plot(list_rounds * step, list_van, color = Dict_Legendes[protocole]["color"], label = Dict_Legendes[protocole]["label"], linewidth = Dict_Legendes[protocole]["linewidth"], ls = Dict_Legendes[protocole]["ls"])

    plt.xlabel("Learning Rounds", fontsize = 15)
    topic = "Model's Rebuild "
    plt.ylabel(topic+" Accuracy ", fontsize = 15)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))#loc = "upper left"
    plt.grid(True)
    #plt.ylim(ymin=0, ymax=3)
    plt.savefig("Images\\fig8_{}.png".format("Unmixnet"), bbox_inches="tight")
    plt.show()

def plot_model_f7(pb, method, key_method = "", Builder = PrivacyBuilder):
    if method in "Classif":
        Experiences = Builder(pb, PathFile = "Acc"+method, key_method = key_method)
    elif method == "DSim":
        Experiences = DSimBuilder(pb, PathFile = "Acc"+method, key_method = key_method)
    elif method == "Link":
        Experiences = LBuilder(pb, PathFile = "Acc"+method, key_method = key_method)
        
    step = 1
    if method in ["Link", "DSim"]:
        step = 5

    for protocole in list(Experiences.keys()):
        list_rounds = np.array(list(range(len(Experiences[protocole]))))
        if method == "DSim":
            list_van = Experiences[protocole]
        else:
            list_van = [np.mean(Experiences[protocole][rounds]) for rounds in list_rounds]
        if protocole!="MFedAvg" and not(protocole == "Noised" and pb == "MotionSense"):
            print("oui")
            print(protocole, list_rounds * step, list_van)
            plt.plot(list_rounds * step, list_van, color = Dict_Legendes[protocole]["color"], label = Dict_Legendes[protocole]["label"], linewidth = Dict_Legendes[protocole]["linewidth"], ls = Dict_Legendes[protocole]["ls"])
        else:
            if pb in ["Cifar10","LFW"] or (protocole == "Noised" and pb == "MotionSense"):
                stip=5
            else:
                stip=10
            print("non")
            print(protocole, list(range(1,41,stip)), list_van)
            plt.plot(list(range(0,41,stip)), list_van[:-1], color = Dict_Legendes[protocole]["color"], label = Dict_Legendes[protocole]["label"], linewidth = Dict_Legendes[protocole]["linewidth"], ls = Dict_Legendes[protocole]["ls"])

    dict_setup = setup(pb)
    if method in ["Link"]:
        rand = 1/ dict_setup["hparams"]["K"]
    elif method in ["Classif", "DSim"] :
        rand, _ = evaluate_random(dict_setup)
    protocole = "Rand"

    plt.plot(list(range(40)), np.ones(40)*rand, color = Dict_Legendes[protocole]["color"], label = Dict_Legendes[protocole]["label"], linewidth = Dict_Legendes[protocole]["linewidth"], ls = Dict_Legendes[protocole]["ls"])

    plt.xlabel("Learning Rounds", fontsize = 15)
    if method in ['DSim', 'Classif']:
        topic = "Attribute"
    elif method in ['Link']:
        topic = "Linkability"
    plt.ylabel(topic+" Inference Accuracy ", fontsize = 15)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))#loc = "upper left"
    plt.grid(True)
    plt.xlim(xmin=-0.1, xmax=40.1)
    plt.savefig("Images\\fig7{}_{}.png".format(method + key_method,pb), bbox_inches="tight")
    plt.show()



def plot_model_f7CDF(pb, method, size = 100, key_method = ""):
    scales = (np.array(list(range(1,size+1)))/size).tolist()
    if method == "Classif":
        Experiences = ClassifCDFPrivacyBuilder(pb, PathFile = "Acc"+method)
    elif method in "DSim":
        Experiences = CDFPrivacyBuilder(pb, PathFile = "Acc"+method)
    elif method == "Link":
        Experiences = LCDFPrivacyBuilder(pb, PathFile = "Acc"+method)
    for protocole in list(Experiences.keys()):
        list_client = list(Experiences[protocole].keys())
        nb_tot = len(list_client)
        list_acc = np.array([Experiences[protocole][str(client)] for client in list_client])

        plt.plot(scales, [[np.mean(sum(list_acc<acc_model))/nb_tot for acc_model in scales][rounds] for rounds in range(len(scales))], color = Dict_Legendes[protocole]["color"], label = Dict_Legendes[protocole]["label"], linewidth = Dict_Legendes[protocole]["linewidth"], ls = Dict_Legendes[protocole]["ls"])
    
    protocole = "Rand"
    if method in ["Link"]:
        rand_evaluate = list_client[0]
        list_rand_evaluate = np.array([np.sum(list_client == rand_evaluate) for client in list_client])
    elif method in ["Classif", "DSim"] :
        _, rand_evaluate = evaluate_random(setup(pb))
        list_rand_evaluate = np.array([setup(pb)["dataset"][client]["metadata"][setup(pb)["attribute"]] == rand_evaluate for client in list_client])
    
    
    
    
    plt.plot(scales, [[np.mean(sum(list_rand_evaluate<acc_model))/nb_tot for acc_model in scales][rounds] for rounds in range(len(scales))], color = Dict_Legendes[protocole]["color"], label = Dict_Legendes[protocole]["label"], linewidth = Dict_Legendes[protocole]["linewidth"], ls = Dict_Legendes[protocole]["ls"])
    plt.xlabel("Ratio of Clients", fontsize = 15)
    if method in ['DSim', 'Classif']:
        topic = "Attribute"
    elif method in ['Link']:
        topic = "Linkability"
    plt.ylabel(topic+" Inference Accuracy ", fontsize = 15)
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))#loc = "upper left"
    plt.grid(True)
    plt.savefig("Images\\fig7CDF_{}_{}.png".format(method + key_method,pb), bbox_inches="tight")
    plt.show()

#plot_model_f7("LFW", "Link")
#plot_model_f7("MotionSense", "DSim")
#plot_model_f5("MotionSense")

# plot_model_f7("LFW", "DSim", "EqDSim")
#plot_model_f7("LFW", "Classif", "CanonClassif")
#plot_model_f5("MotionSense")

# plot_model_f7("Cifar10", "Classif", "ReducedClassif", PrivVanBuilder)




# plot_model_f5("Cifar10")
# plot_model_f5("MotionSense")
# plot_model_f5("MobiAct")
# plot_model_f5("LFW")


# plot_model_f6("Cifar10")
# plot_model_f6("MotionSense")
# plot_model_f6("MobiAct")
# plot_model_f6("LFW")

# plot_model_f7("MotionSense", "Link")
# plot_model_f7("Cifar10", "Link")

# plot_model_f7("MobiAct", "Link")
# plot_model_f7("LFW", "Link")


# plot_model_f7CDF("Cifar10", "Link")
# plot_model_f7CDF("MotionSense", "Link")
# plot_model_f7CDF("MobiAct", "Link")
# plot_model_f7CDF("LFW", "Link")

plot_unmixnet()


def evaluate_model_f7(method, key_method = "", Builder = PrivacyBuilder):
    out = {protocole:list() for protocole in ["FedAvg", "16bit", "8bit", "Noised"]}
    for pb in ["Cifar10", "MotionSense", "MobiAct", "LFW"]:
        out_dict = {}
        if method in "Classif":
            Experiences = Builder(pb, PathFile = "Acc"+method, key_method = key_method)
        elif method == "DSim":
            Experiences = DSimBuilder(pb, PathFile = "Acc"+method, key_method = key_method)
        elif method == "Link":
            Experiences = LBuilder(pb, PathFile = "Acc"+method, key_method = key_method)
            
        step = 1
        if method in ["Link", "DSim"]:
            step = 5
    
        for protocole in list(Experiences.keys()):
            list_rounds = np.array(list(range(len(Experiences[protocole]))))
            list_round = np.array(list(range(len(Experiences[protocole]))))*step
            if method == "DSim":
                list_van = Experiences[protocole]
            else:
                list_van = [np.mean(Experiences[protocole][rounds]) for rounds in list_rounds]
            if protocole!="MFedAvg" and not(protocole == "Noised" and pb == "MotionSense"):
                pass
            else:
                if pb in ["Cifar10","LFW"] or (protocole == "Noised" and pb == "MotionSense"):
                    stip=5
                else:
                    stip=10
                list_round = np.array(list(range(0,40,stip)))
            out_dict[protocole] = {rounds : list_van[i] for i, rounds in enumerate(list_round)}
        
        for protocole in ["FedAvg", "16bit", "8bit", "Noised"]:
            out[protocole].append(np.mean(np.array([out_dict["MFedAvg"][rounds] - out_dict[protocole][rounds] for rounds in out_dict["MFedAvg"].keys()])))
    
    return {protocole : np.mean(out[protocole]) for protocole in out.keys()}

# print(evaluate_model_f7("Link"))

plot_model_f5("MobiAct")