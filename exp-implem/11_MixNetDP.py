from Method import FederatedMixnet
from setup import setup, protocole
import pickle
from pathlib import Path

import sys
try:
    Method, Problem, batch_CV = sys.argv[1], sys.argv[2], sys.argv[3]
    save = True
except:
    Method = "FedAvg"  # "FedAvg","FedNoises","FedMed","FedPruning16bit"
    Problem = "LFW"  # "Cifar10","MotionSense","MobiAct","LFW"
    save = True
config = setup(Problem, dp_bool=True, dp_side='client', only_clip=True)
config_method = protocole(Method)

# if save==False:
#     config['hparams']["K"] = 6
#     config['hparams']["N"] = 5

Federated = FederatedMixnet(config, config_method)
Federated.compute()

model_name = "{}_MixnetDP_E{}_B{}_N{}_K{}.json".format(
    Problem+Method, config["hparams"]["E"], config["hparams"]["B"], config["hparams"]["N"], config["hparams"]["K"])

if save:
    pickle.dump(Federated.Accuracy, open(Path("AccMixnet", "0Van_"+model_name), 'wb'))
    pickle.dump(Federated.MixAccuracy, open(Path("AccMixnet", "1Mix_"+model_name), 'wb'))
