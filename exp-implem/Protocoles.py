from cmath import nan
from random import sample
from tabnanny import verbose
import numpy as np
import matplotlib.pyplot as plt
from pickle import dump, load
from copy import deepcopy
from Method import Canonical
from tqdm import tqdm

# Make path compatible for windows and POSIX compliant systems
from pathlib import Path


class FedAvg(object):
    def __init__(self, setup_dict, list_cli=None):
        self.data = setup_dict["dataset"]
        self.model = setup_dict["model"]
        self.agg_weights = self.model.get_weights()
        self.ini_weights = self.model.get_weights()
        self.hparams = setup_dict["hparams"]
        if list_cli == None:
            self.list_client = list(self.data.keys())
        else:
            self.list_client = list_cli
        self.Accuracy = {client: [0 for i in range(self.hparams['N'])]
                         for client in self.list_client}

    def update_client(self, client, weights):
        #print("Client {} selected for FedAvg model update".format(client))

        self.model.set_weights(weights)
        self.model.fit(self.data[str(client)]['x_train'], self.data[str(client)]['y_train'], epochs=self.hparams['E'],
                       batch_size=self.hparams['B'], validation_data=(self.data[str(client)]['x_test'], self.data[str(client)]['y_test']), verbose=0)
        update = self.model.get_weights()
        nb_layers = len(weights)
        for num_layer in range(nb_layers):
            update[num_layer] += -weights[num_layer]
        return update

    def aggregate_updates(self, Update):
        nb_layers = len(self.agg_weights)
        selected_clients = list(Update.keys())
        n_tot = sum([len(self.data[str(id_client)]['x_train']) for id_client in selected_clients])
        out_weights = deepcopy(self.agg_weights)
        for n_layer in range(nb_layers):
            layer = self.agg_weights[n_layer]

            for id_client in selected_clients:
                layer += (len(self.data[str(id_client)]['x_train'])/n_tot) * \
                    Update[str(id_client)][n_layer]

            out_weights[n_layer] = layer
        return out_weights

    def evaluate_model(self, i, init_weight=None, list_cli=None):
        if init_weight == None:
            self.agg_weights = self.ini_weights
        else:
            self.agg_weights = init_weight
        if list_cli == None:
            list_cli = self.list_client
        self.model.set_weights(self.agg_weights)

        for id_client in list_cli:
            _, acc_client = self.model.evaluate(self.data[str(id_client)]['x_test'], self.data[str(
                id_client)]['y_test'], batch_size=self.hparams['B'])
            self.Accuracy[id_client][i] = acc_client
            print("Aggregated model precision, Client {} : {}\n".format(id_client, acc_client))

    def compute(self, init_weight=None, list_cli=None, round_save=0, name_save=""):

        if init_weight == None:
            self.agg_weights = self.ini_weights
        else:
            self.agg_weights = init_weight
        if list_cli == None:
            self.list_client = list(self.data.keys())
        else:
            self.list_client = list_cli

        # Learning Rounds
        K = min(self.hparams['K'], len(self.list_client))
        for i_round, num_round in tqdm(enumerate(range(1, self.hparams['N']+1))):
            print("\n\n######################################################################\n################## Learning round {} ################################\n######################################################################\n\n".format(
                ("000"+str(num_round))[-3:]))

            selected_clients = sample(self.list_client, K)

            # Federated Learning : client side
            Updates = {id_client: self.update_client(
                id_client, self.agg_weights) for id_client in tqdm(selected_clients)}

            # Federated Learning : Server side
            self.agg_weights = self.aggregate_updates(Updates)

            # Federated Learning : Aggregated model evaluation
            self.evaluate_model(i_round)

            if round_save != 0 and ((i_round/round_save - i_round//round_save) == 0.0 or i_round == 0):
                dump(self.agg_weights, open(
                    Path("Weights_save", "Weights_{}_R{}.sav".format(name_save, i_round)), 'wb'))

    def plot_model(self, name_image):
        dict_accuracy = self.Accuracy
        list_client = list(dict_accuracy.keys())
        list_mean = list()
        list_rounds = list(range(len(dict_accuracy[str(list_client[0])])))
        for rounds in list_rounds:
            temp = list()

            for client in list_client:
                temp.append(dict_accuracy[str(client)][rounds])

            list_mean.append(np.mean(temp))

        plt.plot(list_rounds, list_mean)
        plt.xlabel("Learning rounds", fontsize=15)
        plt.ylabel("Accuracy", fontsize=15)
        plt.grid(True)
        plt.savefig(name_image)
        plt.show()


class FedNoises(FedAvg):
    def __init__(self, setup_dict, V=0.05, list_cli=None):
        FedAvg.__init__(self, setup_dict, list_cli=list_cli)
        self.name_model = "Nsd_"
        self.Var = V

    def update_client(self, client, weights):
        print("Client {} selected for FedNoises model update".format(client))
        self.model.set_weights(weights)
        self.model.fit(self.data[str(client)]['x_train'], self.data[str(client)]['y_train'], epochs=self.hparams['E'],
                       batch_size=self.hparams['B'], validation_data=(self.data[str(client)]['x_test'], self.data[str(client)]['y_test']))
        update = self.model.get_weights()
        nb_layers = len(weights)

        for num_layer in range(nb_layers):
            update[num_layer] += -weights[num_layer] + \
                np.random.normal(0, self.Var, weights[num_layer].shape)
        return update


class FedNoises1(FedNoises):
    def __init__(self, setup_dict, list_cli=None):
        FedNoises.__init__(self, setup_dict, V=0.1, list_cli=None)


class FedMed(FedAvg):
    def aggregate_updates(self, Update):
        print("\nAggregating by using the median.\n")
        return [self.agg_weights[n_layer] + np.median(np.stack([Update[str(id_client)][n_layer] for id_client in list(Update.keys())]), axis=0) for n_layer in range(len(self.agg_weights))]


class FedPer(FedAvg):
    def __init__(self, setup_dict, size_agg, list_cli=None):
        FedAvg.__init__(self, setup_dict, list_cli=list_cli)
        self.weight_cli = {client: deepcopy(self.ini_weights) for client in self.list_client}
        self.size_agg = size_agg

    def update_client(self, client, weights):
        print("Client {} selected for FedPer model update".format(client))

        w_client = deepcopy(self.weight_cli[client])

        # self.model.set_weights(self.weight_cli[client])
        for i in range(self.size_agg):
            w_client[i] = deepcopy(weights[i])

        self.model.set_weights(w_client)
        self.model.fit(self.data[str(client)]['x_train'], self.data[str(client)]['y_train'], epochs=self.hparams['E'],
                       batch_size=self.hparams['B'], validation_data=(self.data[str(client)]['x_test'], self.data[str(client)]['y_test']))
        wgt = self.model.get_weights()

        self.weight_cli[client] = self.model.get_weights()

        update = list()
        for num_layer in range(self.size_agg):
            update.append(wgt[num_layer] - weights[num_layer])
        return update

    def aggregate_updates(self, Update):

        selected_clients = list(Update.keys())
        # total size of visited datasets, usefull for aggregation
        n_tot = sum([len(self.data[str(id_client)]['x_train']) for id_client in selected_clients])
        out_weights = deepcopy(self.agg_weights)
        for n_layer in range(self.size_agg):
            layer = self.agg_weights[n_layer]

            for id_client in selected_clients:
                layer += (len(self.data[str(id_client)]['x_train'])/n_tot) * \
                    Update[str(id_client)][n_layer]

            out_weights[n_layer] = layer
        return out_weights

    def evaluate_model(self, i):
        for id_client in self.list_client:
            w_client = deepcopy(self.weight_cli[id_client])

        # self.model.set_weights(self.weight_cli[client])
            for num_layer in range(self.size_agg):
                w_client[num_layer] = deepcopy(self.agg_weights[num_layer])

            self.model.set_weights(w_client)
            _, acc_client = self.model.evaluate(
                self.data[str(id_client)]['x_test'], self.data[str(id_client)]['y_test'])
            self.Accuracy[id_client][i] = acc_client
            print("Aggregated model precision, Client {} : {}\n".format(id_client, acc_client))


class FedPruning(FedAvg):
    def __init__(self, setup_dict, zeros_ratio=0.2, list_cli=None):
        from tensorflow_model_optimization.sparsity.keras import ConstantSparsity
        import tensorflow_model_optimization as tfmot
        FedAvg.__init__(self, setup_dict, list_cli=list_cli)
        pruning_params = {'pruning_schedule': ConstantSparsity(
            zeros_ratio, 0), 'block_size': (1, 1), 'block_pooling_type': 'AVG'}
        prune_low_magnitude = tfmot.sparsity.keras.prune_low_magnitude
        import tempfile
        logdir = tempfile.mkdtemp()
        self.callbacks = [tfmot.sparsity.keras.UpdatePruningStep(
        ), tfmot.sparsity.keras.PruningSummaries(log_dir=logdir)]
        self.model = prune_low_magnitude(self.model, **pruning_params)
        self.model.compile(optimizer="Adam", loss='categorical_crossentropy', metrics=['accuracy'])
        self.name_model = "Pnd{}_".format(zeros_ratio)
        self.agg_weights = self.model.get_weights()

    def update_client(self, client, weights):
        print("Client {} selected for FedPruning model update".format(client))
        self.model.set_weights(weights)
        self.model.fit(self.data[str(client)]['x_train'], self.data[str(client)]['y_train'], epochs=self.hparams['E'], batch_size=self.hparams['B'], validation_data=(
            self.data[str(client)]['x_test'], self.data[str(client)]['y_test']), callbacks=self.callbacks)
        update = self.model.get_weights()
        nb_layers = len(weights)
        for num_layer in range(nb_layers):
            update[num_layer] += -weights[num_layer]
        return update


class FedPruningDtype(FedAvg):
    def __init__(self, setup_dict, type_array, list_cli=None):
        FedAvg.__init__(self, setup_dict, list_cli=list_cli)
        self.name_model = "Pnd{}_".format(str(type_array))
        self.agg_weights = self.model.get_weights()
        self.type_array = type_array

    def update_client(self, client, weights):
        print("Client {} selected for FedPruning model update".format(client))
        self.model.set_weights(weights)
        self.model.fit(self.data[str(client)]['x_train'], self.data[str(client)]['y_train'], epochs=self.hparams['E'],
                       batch_size=self.hparams['B'], validation_data=(self.data[str(client)]['x_test'], self.data[str(client)]['y_test']))
        update = self.model.get_weights()
        nb_layers = len(weights)
        for num_layer in range(nb_layers):
            update[num_layer] = (update[num_layer] - weights[num_layer]).astype(self.type_array)
        return update

    def aggregate_updates(self, Update):
        nb_layers = len(self.agg_weights)
        selected_clients = list(Update.keys())
        n_tot = sum([len(self.data[str(id_client)]['x_train']) for id_client in selected_clients])
        out_weights = deepcopy(self.agg_weights)
        for n_layer in range(nb_layers):
            layer = self.agg_weights[n_layer]

            for id_client in selected_clients:
                layer += (len(self.data[str(id_client)]['x_train'])/n_tot) * \
                    Update[str(id_client)][n_layer]

            out_weights[n_layer] = layer.astype(self.type_array)
        return out_weights


class FedPruning16bit(FedPruningDtype):
    def __init__(self, setup_dict, list_cli=None):
        FedPruningDtype.__init__(self, setup_dict, np.float16, list_cli=None)


class FedPruning32bit(FedPruningDtype):
    def __init__(self, setup_dict, list_cli=None):
        FedPruningDtype.__init__(self, setup_dict, np.float32, list_cli=None)


class FedPruning64bit(FedPruningDtype):
    def __init__(self, setup_dict, list_cli=None):
        FedPruningDtype.__init__(self, setup_dict, np.float64, list_cli=None)


class FedPruning8bit(FedAvg):
    def __init__(self, setup_dict, list_cli=None):
        FedAvg.__init__(self, setup_dict, list_cli=list_cli)
        self.name_model = "Pnd8bit_"
        self.agg_weights = self.model.get_weights()

    def update_client(self, client, weights):
        print("Client {} selected for FedPruning model update".format(client))
        self.model.set_weights(weights)
        self.model.fit(self.data[str(client)]['x_train'], self.data[str(client)]['y_train'], epochs=self.hparams['E'],
                       batch_size=self.hparams['B'], validation_data=(self.data[str(client)]['x_test'], self.data[str(client)]['y_test']))
        update = self.model.get_weights()
        nb_layers = len(weights)
        for num_layer in range(nb_layers):
            update[num_layer] = np.round((update[num_layer] - weights[num_layer])*256)/256
        return update

    def aggregate_updates(self, Update):
        nb_layers = len(self.agg_weights)
        selected_clients = list(Update.keys())
        n_tot = sum([len(self.data[str(id_client)]['x_train']) for id_client in selected_clients])
        out_weights = deepcopy(self.agg_weights)
        for n_layer in range(nb_layers):
            layer = self.agg_weights[n_layer]

            for id_client in selected_clients:
                layer += (len(self.data[str(id_client)]['x_train'])/n_tot) * \
                    Update[str(id_client)][n_layer]

            out_weights[n_layer] = np.round(layer*256)/256
        return out_weights


class FedAvgCanonical(FedAvg):
    def compute(self, init_weight=None, list_cli=None):

        if init_weight == None:
            self.agg_weights = self.ini_weights
        else:
            self.agg_weights = init_weight
        if list_cli == None:
            self.list_client = list(self.data.keys())
        else:
            self.list_client = list_cli

        # Learning Rounds
        K = min(self.hparams['K'], len(self.list_client))
        for i_round, num_round in enumerate(range(1, self.hparams['N']+1)):
            print("\n\n######################################################################\n################## Learning round {} ################################\n######################################################################\n\n".format(
                ("000"+str(num_round))[-3:]))

            selected_clients = sample(self.list_client, K)

            # Federated Learning : client side
            Updates = {id_client: Canonical(self.update_client(
                id_client, self.agg_weights), flat_form=False) for id_client in selected_clients}

            # Federated Learning : Server side
            self.agg_weights = self.aggregate_updates(Updates)

            # Federated Learning : Aggregated model evaluation
            self.evaluate_model(i_round)
