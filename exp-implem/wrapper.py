from torch.utils.data import Dataset, DataLoader
from opacus.utils.batch_memory_manager import BatchMemoryManager
from torch import nn
import torch
import numpy as np

class MSDataset(Dataset):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __len__(self):
        return np.shape(self.y)[0]

    def __getitem__(self, idx):
        return self.x[idx,:,:,:], self.y[idx,:]


def pytorch_image(x):
    #images in pytorch are not shaped like image in tf 
    #in tf we have [N, W, H, C] (number of images, width, height, num of channel)
    #in pytorch we have [N, C, W, H] 
    #in addition in pytorch conv2d only works with 4 dimentional tensors 
        shape = np.shape(x)
        return x.reshape([shape[0], shape[3], np.shape(x)[1], np.shape(x)[2]])

def accuracy(preds, labels):
    return (preds == labels.argmax(1)).mean()

class model():
    def __init__(self, model,MAX_GRAD_NORM=1.2,EPSILON=50,DELTA=1e-5,MAX_PHYSICAL_BATCH_SIZE=128):
        self.model = model
        self.archi = model.archi
        self.keys = model.state_dict().keys()

        self.MAX_GRAD_NORM = MAX_GRAD_NORM
        self.EPSILON = EPSILON
        self.DELTA = DELTA
        self.MAX_PHYSICAL_BATCH_SIZE = MAX_PHYSICAL_BATCH_SIZE
    
    def set_params(self,MAX_GRAD_NORM=1.2,EPSILON=50,DELTA=1e-5,MAX_PHYSICAL_BATCH_SIZE=128):
        self.MAX_GRAD_NORM = MAX_GRAD_NORM
        self.EPSILON = EPSILON
        self.DELTA = DELTA
        self.MAX_PHYSICAL_BATCH_SIZE = MAX_PHYSICAL_BATCH_SIZE
        
    def fit(self, x, y, epochs, batch_size, validation_data, verbose):

        def train(model, train_loader, optimizer, epoch):
            model.train()
            criterion = nn.CrossEntropyLoss()

            losses = []
            top1_acc = []
            
            with BatchMemoryManager(
                data_loader=train_loader, 
                max_physical_batch_size=self.MAX_PHYSICAL_BATCH_SIZE, 
                optimizer=optimizer
            ) as memory_safe_data_loader:

                for i, (images, target) in enumerate(memory_safe_data_loader):   
                    images = images.float()
                    optimizer.zero_grad()

                    # compute output
                    output = model(images)

                    loss = criterion(output, target)

                    preds = np.argmax(output.detach().cpu().numpy(), axis=1)
                    labels = target.detach().cpu().numpy()

                    # measure accuracy and record loss
                    acc = accuracy(preds, labels)

                    losses.append(loss.item())
                    top1_acc.append(acc)

                    loss.backward()
                    optimizer.step()

                    if (i+1) % 200 == 0:
                        epsilon = privacy_engine.get_epsilon(self.DELTA)
                        print(
                            f"\tTrain Epoch: {epoch} \t"
                            f"Loss: {np.mean(losses):.6f} "
                            f"Acc@1: {np.mean(top1_acc) * 100:.6f} "
                            f"(ε = {epsilon:.2f}, δ = {self.DELTA})"
                        )

        # print("############################")
        # print(np.shape(x))

        train_dataset = MSDataset(pytorch_image(x), y)
        test_dataset = MSDataset(pytorch_image(validation_data[0]), validation_data[1])

        trainloader = DataLoader(train_dataset, batch_size=batch_size)
        testloader = DataLoader(test_dataset, batch_size=batch_size)

        optimizer = torch.optim.Adam(self.model.parameters())

        from opacus import PrivacyEngine

        privacy_engine = PrivacyEngine()


        # print(f"################ before = {self.model}")
        model, optimizer, train_loader = privacy_engine.make_private_with_epsilon(
            module=self.model,
            optimizer=optimizer,
            data_loader=trainloader,
            epochs=epochs,
            target_epsilon=self.EPSILON,
            target_delta=self.DELTA,
            max_grad_norm=self.MAX_GRAD_NORM,
        )
        # print(f"################ after = {self.model}")
        # print(f"################ after = {model}")

        for epoch in range(epochs):
            train(model, train_loader, optimizer, epoch + 1)

        
        w = []
        d = model.state_dict()
        for k in d.keys():
            w += [d[k]]
        w = np.array(w, dtype=object)
        self.set_weights(w)


        #top1_acc = test(model, testloader)

    def evaluate(self, x, y, batch_size=258):
        test = MSDataset(pytorch_image(x),y)
        testloader = DataLoader(test, batch_size=batch_size)

        def test(model, test_loader):
            model.eval()
            criterion = nn.CrossEntropyLoss()
            losses = []
            top1_acc = []

            with torch.no_grad():
                for images, target in test_loader:
                    images = images.float()

                    output = model(images)
                    loss = criterion(output, target)
                    preds = np.argmax(output.detach().cpu().numpy(), axis=1)
                    labels = target.detach().cpu().numpy()
                    acc = accuracy(preds, labels)

                    losses.append(loss.item())
                    top1_acc.append(acc)

            top1_avg = np.mean(top1_acc)

            print(
                f"\tTest set:"
                f"Loss: {np.mean(losses):.6f} "
                f"Acc: {top1_avg * 100:.6f} "
            )
            return (0,np.mean(top1_acc))

        top1_acc = test(self.model, testloader)
        return top1_acc





    def get_weights(self):
        w = []
        for k in self.keys:
            w += [self.model.state_dict()[k]]
        w = np.array(w, dtype=object)
        return w


    def set_weights(self, weights):
        class MSNeuralNetwork(nn.Module):
            def __init__(self, archi):
                super(MSNeuralNetwork, self).__init__()
                self.archi = archi

            def forward(self, x):
                logits = self.archi(x)
                return logits

        self.model = MSNeuralNetwork(self.archi)
        d = {}
        for i,k in enumerate(self.keys):
                d[k] = weights[i]
        self.model.load_state_dict(d)
