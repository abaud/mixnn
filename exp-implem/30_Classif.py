from Method import ClassifAugmAttack
from setup import setup, protocole
import pickle
from numpy.random import shuffle
from numpy import array
from pathlib import Path
import numpy

import sys
try :
    Method, Problem, batch_CV, num_round = sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]
except:
    Method = "FedAvg"#"FedAvg","FedNoises","FedMed","FedPruning16bit"
    Problem = "MotionSense"#"Cifar10","MotionSense","MobiAct","LFW"
    batch_CV = "b0"#"b0","b1","b2"
    num_round = 0
num_round = int(num_round)
config = setup(Problem)
config_method = protocole(Method)
clients = array(list(config["dataset"].keys()))
config["hparams"]["E"]=10
config["hparams"]["N"]=20
def split_client_cross_val(dataset):
    dict_split = {"train" : {"b0" : list(), "b1" : list(), "b2" : list(), "b3" : list()}, "test" : {"b0" : list(), "b1" : list(), "b2" : list(), "b3" : list()}}
    list_cli = numpy.array(list(dataset.keys()))
    list_mod = numpy.array([dataset[client]["metadata"][config["attribute"]] for client in list_cli])
    modalities = numpy.unique(list_mod)
    dic_distrib = {mod : list_cli[list_mod==mod] for mod in modalities}
    for mod in modalities:
        dict_split["test"]["b0"] += list(dic_distrib[mod][:3])
        dict_split["train"]["b0"] += list(dic_distrib[mod][3:])
    return dict_split

split_dict = split_client_cross_val(config["dataset"])
list_aux, list_cli = split_dict["test"][batch_CV], split_dict["train"][batch_CV]
ini_weights = pickle.load( open(Path("Weights_save", "Weights_{}_R{}.sav".format(Problem, num_round)), 'rb') )
Federated = ClassifAugmAttack(config, config_method, list_cli, list_aux, config["attribute"], n_augm = 8, save = True, name_pb = Problem + "V5")
Federated.compute(init_weight = ini_weights, starting_round=num_round, inter_save=5)

model_name = "{}_AttBaseline_R{}_E{}_B{}_N{}_K{}.sav".format(Problem+Method+batch_CV, num_round, config["hparams"]["E"], config["hparams"]["B"], config["hparams"]["N"], config["hparams"]["K"])
pickle.dump(Federated.acc_attack, open(Path("AccClassif","0Mean_"+model_name), 'wb'))
pickle.dump(Federated.perf_client, open(Path("AccClassif","1CDF_"+model_name), 'wb'))