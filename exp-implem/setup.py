
from pathlib import Path
import pickle
from typing import Optional


def ModelCifar10(optimizer='Adam'):
    from keras.models import Sequential
    from keras.layers import Conv2D
    from keras.layers import MaxPooling2D
    from keras.layers import Dense
    from keras.layers import Flatten
    '''
    Returns a convolutional model to solve Cifar10.

            Parameters:
                    none

            Returns:
                    model (model): a convolutional model.
    '''
    model = Sequential()
    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform',
              padding='same', input_shape=(32, 32, 3)))
    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Flatten())
    model.add(Dense(200, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dense(10, activation='softmax'))
    # compile model

    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    return model


def ModelCifar10Opacus(MAX_GRAD_NORM=1.2, EPSILON=50, DELTA=1e-5, MAX_PHYSICAL_BATCH_SIZE=128):
    def model():
        import wrapper as w
        from torch import nn

        class MSNeuralNetwork(nn.Module):
            def __init__(self):
                super(MSNeuralNetwork, self).__init__()
                self.archi = nn.Sequential(
                    nn.Conv2d(3, 64, (3, 3), padding=(1, 1)),
                    nn.Conv2d(64, 64, (3, 3), padding=(1, 1)),
                    nn.MaxPool2d((2, 2)),
                    nn.Flatten(),
                    nn.Linear(16384, 200),
                    nn.Linear(200, 10)
                )

            def forward(self, x):
                return self.archi(x)

        return w.model(MSNeuralNetwork(), MAX_GRAD_NORM, EPSILON, DELTA, MAX_PHYSICAL_BATCH_SIZE)
    return model


def ModelMotionSense(optimizer='Adam'):
    '''
    Returns a convolutional model to solve MotionSense.

            Parameters:
                    none

            Returns:
                    model (model): a convolutional model.
    '''
    from keras.models import Sequential
    from keras.layers import Conv2D
    from keras.layers import MaxPooling2D
    from keras.layers import Dense
    from keras.layers import Flatten
    model = Sequential()
    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform',
              padding='same', input_shape=(12, 50, 1)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dense(1000, activation='relu', kernel_initializer='he_uniform'))
    model.add(Flatten())
    model.add(Dense(500, activation='relu', kernel_initializer='he_uniform'))

    model.add(Dense(500, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dense(4, activation='softmax'))
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    return model


def ModelMotionSenseOpacus(MAX_GRAD_NORM=1.2, EPSILON=50, DELTA=1e-5, MAX_PHYSICAL_BATCH_SIZE=128):
    def model():
        import wrapper as w
        from torch import nn

        class MSNeuralNetwork(nn.Module):
            def __init__(self):
                super(MSNeuralNetwork, self).__init__()
                self.archi = nn.Sequential(
                    nn.Conv2d(1, 32, (3, 3), padding=(1, 1)),
                    nn.MaxPool2d((2, 2)),
                    nn.Conv2d(32, 32, (3, 3), padding=(1, 1)),
                    nn.MaxPool2d((2, 2)),
                    nn.Flatten(),
                    nn.Linear(1152, 1000),
                    nn.Linear(1000, 500),
                    nn.Linear(500, 500),
                    nn.Linear(500, 4)
                )

            def forward(self, x):
                logits = self.archi(x)
                return logits

        return w.model(MSNeuralNetwork(), MAX_GRAD_NORM, EPSILON, DELTA, MAX_PHYSICAL_BATCH_SIZE)
    return model


def ModelMobiAct(optimizer='Adam'):
    '''
    Returns a convolutional model to solve MobiAct.

            Parameters:
                    none

            Returns:
                    model (model): a convolutional model.
    '''
    from keras.models import Sequential
    from keras.layers import Conv2D
    from keras.layers import MaxPooling2D
    from keras.layers import Dense
    from keras.layers import Flatten
    model = Sequential()
    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform',
              padding='same', input_shape=(6, 20, 1)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dense(1000, activation='relu', kernel_initializer='he_uniform'))  # 1000
    model.add(Dense(500, activation='relu', kernel_initializer='he_uniform'))  # 500
    model.add(Dense(500, activation='relu', kernel_initializer='he_uniform'))  # 100
    model.add(Flatten())

    model.add(Dense(4, activation='softmax'))

    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])
    return model


def ModelMobiActOpacus(MAX_GRAD_NORM=1.2, EPSILON=50, DELTA=1e-5, MAX_PHYSICAL_BATCH_SIZE=128):
    def model():
        import wrapper as w
        from torch import nn

        class MSNeuralNetwork(nn.Module):
            def __init__(self):
                super(MSNeuralNetwork, self).__init__()
                self.archi = nn.Sequential(
                    nn.Conv2d(1, 32, (3, 3), padding=(1, 1)),
                    nn.MaxPool2d((2, 2)),
                    nn.Conv2d(32, 32, (3, 3), padding=(1, 1)),
                    nn.MaxPool2d((2, 2)),
                    nn.Flatten(),
                    nn.Linear(160, 1000),
                    nn.Linear(1000, 500),
                    nn.Linear(500, 500),
                    nn.Linear(500, 4)
                )

            def forward(self, x):
                logits = self.archi(x)
                return logits

        return w.model(MSNeuralNetwork(), MAX_GRAD_NORM, EPSILON, DELTA, MAX_PHYSICAL_BATCH_SIZE)
    return model


def ModelLFW(optimizer='Adam'):
    '''
    Returns a convolutional model to solve Labelled faces in the wild.

            Parameters:
                    none

            Returns:
                    model (model): a convolutional model.
    '''
    from keras.models import Sequential

    from keras.layers import Convolution2D
    from keras.layers import LocallyConnected2D
    from keras.layers import MaxPooling2D

    from keras.layers import Dense
    from keras.layers import Flatten
    model = Sequential()
    model.add(Convolution2D(32, (11, 11), activation='relu', name='C1', input_shape=(32, 32, 3)))
    model.add(MaxPooling2D(pool_size=3, strides=2, padding='same', name='M2'))
    model.add(Convolution2D(16, (9, 9), activation='relu', name='C3'))
    model.add(Flatten(name='F0'))
    model.add(Dense(4096, activation='relu', name='F7'))

    model.add(Dense(3, activation='softmax'))

    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

    return model


def ModelLFWOpacus(MAX_GRAD_NORM=1.2, EPSILON=50, DELTA=1e-5, MAX_PHYSICAL_BATCH_SIZE=128):
    def model():
        import wrapper as w
        from torch import nn

        class MSNeuralNetwork(nn.Module):
            def __init__(self):
                super(MSNeuralNetwork, self).__init__()
                self.archi = nn.Sequential(
                    nn.Conv2d(3, 32, (11, 11)),
                    nn.MaxPool2d((3, 3), stride=2, padding=1),
                    nn.Conv2d(32, 16, (3, 3)),
                    nn.Flatten(),
                    nn.Linear(1296, 4096),
                    nn.Linear(4096, 3)
                )

            def forward(self, x):
                logits = self.archi(x)
                return logits

        return w.model(MSNeuralNetwork(), MAX_GRAD_NORM, EPSILON, DELTA, MAX_PHYSICAL_BATCH_SIZE)

    return model


def ModelAdult(optimizer='Adam'):
    '''
    Returns a convolutional model to solve Adult Dataset.

            Parameters:
                    none

            Returns:
                    model (model): a convolutional model.
    '''
    from keras.models import Sequential

    from keras.layers import Dense
    model = Sequential()
    model.add(Dense(2, activation='relu', input_shape=(37,)))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(100, activation='relu'))

    model.add(Dense(2, activation='softmax'))

    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

    return model


def ModelAdultOpacus(MAX_GRAD_NORM=1.2, EPSILON=50, DELTA=1e-5, MAX_PHYSICAL_BATCH_SIZE=128):
    def model():
        import wrapper as w
        from torch import nn

        class MSNeuralNetwork(nn.Module):
            def __init__(self):
                super(MSNeuralNetwork, self).__init__()
                self.archi = nn.Sequential(
                    nn.Linear(37, 2),
                    nn.Linear(2, 100),
                    nn.Linear(100, 100),
                    nn.Linear(100, 2)
                )

            def forward(self, x):
                logits = self.archi(x)
                return logits

        return w.model(MSNeuralNetwork(), MAX_GRAD_NORM, EPSILON, DELTA, MAX_PHYSICAL_BATCH_SIZE)
    return model


def get_noise_multiplier(num_train: int,
                         epsilon: float,
                         delta: float,
                         epochs: int,
                         batch_size: int,
                         tolerance: float = 1e-2) -> Optional[float]:
    """Computes the noise multiplier for DP-SGD given privacy parameters.

      The algorithm performs binary search on the values of epsilon.

    Args:
      num_train: number of input training points.
      epsilon: epsilon parameter in (epsilon, delta)-DP.
      delta: delta parameter in (epsilon, delta)-DP.
      epochs: number of training epochs.
      batch_size: the number of examples in each batch of gradient descent.
      tolerance: an upper bound on the absolute difference between the input
        (desired) epsilon and the epsilon value corresponding to the
        noise_multiplier that is output.

    Returns:
      noise_multiplier: the smallest noise multiplier value (within plus or
       minus the given tolerance) for which using DPKerasAdamOptimizer will
       result in an (epsilon, delta)-differentially private trained model.
    """
    import dp_accounting
    import math

    orders = ([1.25, 1.5, 1.75, 2., 2.25, 2.5, 3., 3.5, 4., 4.5] +
              list(range(5, 64)) + [128, 256, 512])
    steps = int(math.ceil(epochs * num_train / batch_size))

    def make_event_from_param(noise_multiplier):
        return dp_accounting.SelfComposedDpEvent(
            dp_accounting.PoissonSampledDpEvent(
                sampling_probability=batch_size / num_train,
                event=dp_accounting.GaussianDpEvent(noise_multiplier)), steps)

    return dp_accounting.calibrate_dp_mechanism(
        lambda: dp_accounting.rdp.RdpAccountant(orders),
        make_event_from_param,
        epsilon,
        delta,
        dp_accounting.LowerEndpointAndGuess(0, 1),
        tol=tolerance)


def setup(problem, dp_bool=False, dp_side=None, l2_norm_clip=4., epsilon=10,
          delta=1e-5, only_clip=False, num_microbatches=1):
    '''
    Returns a dictionary which can initialize a federated protocole.

            Parameters:
                    problem (str) : name of the setup you want to initialize, please check the available setup names in the following code.

            Returns:
                    output (dict): - "dataset" contains a decentralized dataset, split between all the clients of the dataset. Each clients has a test-train X-Y splitten datasets and metadata such as gender, etc...
                                   - "model" contains a function to build a tf neural network to solve the dataset.
                                   - "hparams" contains a dictionaray of hyperparameters that provides good results for the federated protocole.
                                   - "attribute" contains the most classic attribute to use about the clients for privacy leakage evaluation
    '''
    from tensorflow_privacy.privacy.optimizers.dp_optimizer_keras import DPKerasAdamOptimizer

    output = {}
    output['dp_side'] = dp_side
    output['noise_multiplier'] = 0
    output['clip_norm'] = l2_norm_clip
    output["dataset"] = pickle.load(open(Path("Data/{}.pickle".format(problem)), "rb"))
    client_rnd = list(output['dataset'].keys())[0]
    num_train = len(output['dataset'][client_rnd]['x_train'])

    def get_dp_optimizer(num_train: int,
                         l2_norm_clip: float,
                         num_microbatches: int,
                         epsilon: float,
                         delta: float,
                         epochs: int,
                         batch_size: int,
                         tolerance: float = 1e-2) -> Optional[float]:
        if only_clip:
            noise_multiplier = 0
        else:
            noise_multiplier = get_noise_multiplier(
                num_train, epsilon, delta, epochs, batch_size, tolerance)
        optimizer = DPKerasAdamOptimizer(
            l2_norm_clip=l2_norm_clip,
            noise_multiplier=noise_multiplier,
            num_microbatches=num_microbatches)

        return optimizer, noise_multiplier

    if problem == "Cifar10":
        if dp_bool:
            if dp_side == 'client':
                optimizer, noise_multiplier = get_dp_optimizer(
                    num_train, l2_norm_clip, num_microbatches, epsilon, delta, 40, 10)
                output["model"] = ModelCifar10(optimizer)
                output['noise_multiplier'] = noise_multiplier
            elif dp_side == 'mixnet':
                output["model"] = ModelCifar10()
                noise_multiplier = get_noise_multiplier(num_train, epsilon, delta, 40, num_train)
                output['noise_multiplier'] = noise_multiplier
        else:
            output["model"] = ModelCifar10()
        output["hparams"] = {'E': 3, 'B': 32, 'N': 40, 'K': 20}
        output["attribute"] = "distrib"
    elif problem == "MotionSense":
        if dp_bool:
            if dp_side == 'client':
                optimizer, noise_multiplier = get_dp_optimizer(
                    num_train, l2_norm_clip, num_microbatches, epsilon, delta, 40, 10)
                output["model"] = ModelMotionSense(optimizer)
                output['noise_multiplier'] = noise_multiplier
            elif dp_side == 'mixnet':
                output["model"] = ModelMotionSense()
                noise_multiplier = get_noise_multiplier(num_train, epsilon, delta, 40, num_train)
                output['noise_multiplier'] = noise_multiplier
        else:
            output["model"] = ModelMotionSense()
        output["hparams"] = {'E': 2, 'B': 256, 'N': 40, 'K': 24}
        output["attribute"] = "gender"
    elif problem == "MobiAct":
        if dp_bool:
            if dp_side == 'client':
                optimizer, noise_multiplier = get_dp_optimizer(
                    num_train, l2_norm_clip, num_microbatches, epsilon, delta, 40, 10)
                output["model"] = ModelMobiAct(optimizer)
                output['noise_multiplier'] = noise_multiplier
            elif dp_side == 'mixnet':
                output["model"] = ModelMobiAct()
                noise_multiplier = get_noise_multiplier(num_train, epsilon, delta, 40, num_train)
                output['noise_multiplier'] = noise_multiplier
        else:
            output["model"] = ModelMobiAct()
        output["hparams"] = {'E': 3, 'B': 256, 'N': 40, 'K': 58}
        output["attribute"] = "gender"
    elif problem == "LFW":
        if dp_bool:
            if dp_side == 'client':
                optimizer, noise_multiplier = get_dp_optimizer(
                    num_train, l2_norm_clip, num_microbatches, epsilon, delta, 40, 10)
                output["model"] = ModelLFW(optimizer)
                output['noise_multiplier'] = noise_multiplier
            elif dp_side == 'mixnet':
                output["model"] = ModelLFW()
                noise_multiplier = get_noise_multiplier(num_train, epsilon, delta, 40, num_train)
                output['noise_multiplier'] = noise_multiplier
        else:
            output["model"] = ModelLFW()
        output["hparams"] = {'E': 2, 'B': 8, 'N': 40, 'K': 62}
        output["attribute"] = "gender"
    elif problem == "Adult":
        if dp_bool:
            if dp_side == 'client':
                optimizer, noise_multiplier = get_dp_optimizer(
                    num_train, l2_norm_clip, num_microbatches, epsilon, delta, 40, 10)
                output["model"] = ModelAdult(optimizer)
                output['noise_multiplier'] = noise_multiplier
            elif dp_side == 'mixnet':
                output["model"] = ModelAdult()
                noise_multiplier = get_noise_multiplier(num_train, epsilon, delta, 40, num_train)
                output['noise_multiplier'] = noise_multiplier
        else:
            output["model"] = ModelAdult()
        output["hparams"] = {'E': 6, 'B': 256, 'N': 40, 'K': 4}
        output["attribute"] = "ed_level"
    else:
        raise NameError('Dataset Undefined.')
    return output


def protocole(method):
    """
    function to bind method (str) to a function to import.
    """

    output = {"name": method}

    if method == "FedAvg":
        from Protocoles import FedAvg
        output["method"] = FedAvg
    elif method == "FedNoises":
        from Protocoles import FedNoises
        output["method"] = FedNoises
    elif method == "FedNoises1":
        from Protocoles import FedNoises1
        output["method"] = FedNoises1
    elif method == "FedMed":
        from Protocoles import FedMed
        output["method"] = FedMed
    elif method == "FedPer":
        from Protocoles import FedPer
        output["method"] = FedPer
    elif method == "FedPruning":
        from Protocoles import FedPruning
        output["method"] = FedPruning
    elif method == "FedPruning16bit":
        from Protocoles import FedPruning16bit
        output["method"] = FedPruning16bit
    elif method == "FedPruning32bit":
        from Protocoles import FedPruning32bit
        output["method"] = FedPruning32bit
    elif method == "FedPruning64bit":
        from Protocoles import FedPruning64bit
        output["method"] = FedPruning64bit
    elif method == "FedPruning8bit":
        from Protocoles import FedPruning8bit
        output["method"] = FedPruning8bit
    elif method == "FedAvgCanonical":
        from Protocoles import FedAvgCanonical
        output["method"] = FedAvgCanonical
    else:
        raise NameError('Protocole Undefined.')
    return output


# def Application(name):
#     output = {"name" : name}
#     if name == "MixNN":
#         from Method import FederatedMixnet
#         output["application"] = FederatedMixnet
#     if name == "Classif":
#         from Method import ClassifAttack
#         output["application"] = ClassifAttack
#     if name == "Linkability":
#         from Method import LinkabilityAttack
#         output["application"] = LinkabilityAttack
#     else:
#         raise NameError('Application Undefined.')
#     return output
